<?php
/* Основная разметка страницы в которую вставляеются логические блоки */
?>
<!DOCTYPE HTML>
<html lang="ru">
<head>
    <meta charset="UTF-8">
	<title><?= $PAGE['title']?></title>
	<link rel="shortcut icon" href="/i/fav.ico" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,600,700,400italic,600italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <?php INSERT($INCLUDES['javascript'],'','');?>
    <?php INSERT($INCLUDES['css'],'','');?>
</head>
<body>
	<div id="layout">
		<?php
			INSERT(
				$INCLUDES['header'],
				$short_file_path,
				$folder
			);
		?>
		<?php
			INSERT(
				$INCLUDES['content'],
				$short_file_path,
				$folder
			);
		?>
		<?php
			INSERT(
				$INCLUDES['footer'],
				$short_file_path,
				$folder
			);
		?>
	</div>

    <?php
        INSERT(
            $INCLUDES['popup'],
            $short_file_path,
            $folder
        );
    ?>
</body>
</html>