<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Блог</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Блог</h2>
                <h4>Узнайте много нового! </h4>
            </hgroup>

            <div class="wrapper">
                <aside class="aside">
                    <div class="sidebar shadow_medium">

                        <section class="sidebar_widget">
                            <header>Категории</header>
                            <nav class="sidebar_menu">
                                <ul class="sidebar_menu_mark_list">
                                    <li class="submenulink curr">
                                        <span>Мастер-классы</span>
                                        <ul class="submenu">
                                            <li>
                                                <a href="#">по лепке цветов</a>
                                            </li>
                                            <li>
                                                <a href="#">по созданию фигурок</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Студия</a>
                                    </li>
                                    <li>
                                        <a href="#">Из жизни</a>
                                    </li>
                                    <li>
                                        <a href="#">Новые работы и идеи</a>
                                    </li>
                                </ul>
                            </nav>
                        </section>

                    </div>
                    <div class="sidebar shadow_medium">

                        <section class="sidebar_widget">
                            <header>Популярные статьи</header>
                            <?php
                                BLOCK('sidebar_slider');
                            ?>
                        </section>

                    </div>
                </aside>
                <section class="section">

                    <article class="blog_post">
                        <div class="blog_post_img">
                            <a href="blog-page.html" class="article_link article_img">
                                <img src="/i/blog-01.jpg" width="323" height="196">
                                <div class="overlap">Открыть статью</div>
                            </a>
                        </div>
                        <div class="blog_post_txt">
                            <h2><a href="blog-page.html">Бутоньерка на свадьбу</a></h2>
                            <p>Современный свадебный хейд-мейд в настоящее время и по большей части ограничивается шитьем рубашки жениха, которую затем подружки продают ненаглядному накануне свадьбы. Предлагаем расширить границы творческого самовыражения невесты — сделать собствнми...</p>
                            <p class="blog_post_meta more">
                                <span>комментариев нет  / 18.08.2013</span>
                                <a href="blog-page.html">Узнать больше</a>
                            </p>
                        </div>
                    </article>

                    <article class="blog_post">
                        <div class="blog_post_img">
                            <a href="blog-page.html" class="article_link article_img">
                                <img src="/i/blog-02.jpg" width="323" height="196">
                                <div class="overlap">Открыть статью</div>
                            </a>
                        </div>
                        <div class="blog_post_txt">
                            <h2><a href="blog-page.html">Делаем своими руками веточку сирени</a></h2>
                            <p>Современный свадебный хейд-мейд в настоящее время и по большей части ограничивается шитьем рубашки жениха, которую затем подружки продают ненаглядному накануне свадьбы. Предлагаем расширить границы творческого </p>
                            <p class="blog_post_meta more">
                                <span>56 комментариев  / 18.08.2013</span>
                                <a href="blog-page.html">Узнать больше</a>
                            </p>
                        </div>
                    </article>

                    <article class="blog_post">
                        <div class="blog_post_img">
                            <a href="blog-page.html" class="article_link article_img">
                                <img src="/i/blog-03.jpg" width="323" height="196">
                                <div class="overlap">Открыть статью</div>
                            </a>
                        </div>
                        <div class="blog_post_txt">
                            <h2><a href="blog-page.html">Повышаем квалификацию мастерства</a></h2>
                            <p>В нашей Школе теперь можно пройти не только основную программу по направлению Цветы - курсы С1 и С2 - и дополнительный курс "Тропические цветы", но еще Инструктора могут повысить свою квалификацию, занимаясь на специальном курсе С3.</p>
                            <p class="blog_post_meta more">
                                <span>127 комментариев  / 18.08.2013</span>
                                <a href="blog-page.html">Узнать больше</a>
                            </p>
                        </div>
                    </article>

                    <article class="blog_post">
                        <div class="blog_post_img">
                            <a href="blog-page.html" class="article_link article_img">
                                <img src="/i/blog-04.jpg" width="323" height="196">
                                <div class="overlap">Открыть статью</div>
                            </a>
                        </div>
                        <div class="blog_post_txt">
                            <h2><a href="blog-page.html">Люстра с розами в программе "Фазенда"</a></h2>
                            <p>Для тех, кто не смог посмотреть 15 января программу "Фазенда" по 1 каналу, я выкладываю здесь ссылку. В этой программе участвовала Ольга Петрова, Инструктор Академии DECO, которая привезла в Россию эту технику. Она декорирует розами люстру! </p>
                            <p class="blog_post_meta more">
                                <span>82 комментария  / 18.08.2013</span>
                                <a href="blog-page.html">Узнать больше</a>
                            </p>
                        </div>
                    </article>

                    <article class="blog_post">
                        <div class="blog_post_img">
                            <a href="blog-page.html" class="article_link article_img">
                                <img src="/i/blog-05.jpg" width="323" height="196">
                                <div class="overlap">Открыть статью</div>
                            </a>
                        </div>
                        <div class="blog_post_txt">
                            <h2><a href="blog-page.html">К нам приезжают гости из Японии для проведения занятий</a></h2>
                            <p>В октябре наша Школа готовится принять Сенсеев Казуко и Юкико Мияй в Санкт-Петербурге! Они проведут несколько занятий по второму курсу скульптуры для Инструкторов.</p>
                            <p class="blog_post_meta more">
                                <span>771 комментарий  / 18.08.2013</span>
                                <a href="blog-page.html">Узнать больше</a>
                            </p>
                        </div>
                    </article>

                    <article class="blog_post">
                        <div class="blog_post_img">
                            <a href="blog-page.html" class="article_link article_img">
                                <img src="/i/blog-06.jpg" width="323" height="196">
                                <div class="overlap">Открыть статью</div>
                            </a>
                        </div>
                        <div class="blog_post_txt">
                            <h2><a href="blog-page.html">26 января - ночь музеев</a></h2>
                            <p>В Омском Государственном Историко-Краеведческом музее, в зале с экспозицией "Храни, меня мой талисман..." провела показательный мастер-класс. Экспозиция представлялет украшения которыми украшали себя наши бабушки-прапрабабушки, а я представила современный вид рукоделия, материал </p>
                            <p class="blog_post_meta more">
                                <span>771 комментарий  / 18.08.2013</span>
                                <a href="blog-page.html">Узнать больше</a>
                            </p>
                        </div>
                    </article>

                    <?php
                        BLOCK('paginator');
                    ?>
                </section>
            </div>

        </div>

    </div>

</div>