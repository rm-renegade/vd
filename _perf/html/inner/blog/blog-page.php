<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <a href="blog.html">Блог</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="blog.html">Мастер-классы</a>
                    <ul class="submenu">
                        <li>
                            <a href="#">по лепке цветов</a>
                        </li>
                        <li>
                            <a href="#">по созданию фигурок</a>
                        </li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <a href="blog.html">по лепке цветов</a>
                </li>
                <li>
                    <span> Бутоньерка на свадьбу</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Блог</h2>
                <h4>Узнайте много нового! </h4>
            </hgroup>

            <div class="wrapper page">
                <aside class="aside">
                    <div class="sidebar shadow_medium">

                        <section class="sidebar_widget">
                            <header>Категории</header>
                            <nav class="sidebar_menu">
                                <ul class="sidebar_menu_mark_list">
                                    <li class="submenulink curr">
                                        <a href="#">Мастер-классы</a>
                                        <ul class="submenu" style="display: block">
                                            <li class="curr">
                                                <span>по лепке цветов</span>
                                            </li>
                                            <li>
                                                <a href="#">по созданию фигурок</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Студия</a>
                                    </li>
                                    <li>
                                        <a href="#">Из жизни</a>
                                    </li>
                                    <li>
                                        <a href="#">Новые работы и идеи</a>
                                    </li>
                                </ul>
                            </nav>
                        </section>

                    </div>
                    <div class="sidebar shadow_medium">

                        <section class="sidebar_widget">
                            <header>Популярные статьи</header>
                            <?php
                                BLOCK('sidebar_slider');
                            ?>
                        </section>

                    </div>
                </aside>


                <section class="section">
                    <article class="page_content">
                        <hgroup>
                            <h1>Бутоньерка на свадьбу</h1>
                            <a class="back" href="news.html">Вернуться</a>
                        </hgroup>
                        <section>
                            <div class="page_img_txt">
                                <div class="page_left">
                                    <img src="/i/blog-post-01.jpg" width="375" height="260" border="0">
                                </div>
                                <div class="page_right">
                                    <p>По своим философским взглядам <a href="#">Дезами был материалистом</a> и атеистом, последователем Гельвеция, однако гештальтпсихология иллюзорна.</p>
                                    <p>Интеллект нетривиален. Ощущение мира трогательно наивно. Дистинкция рассматривается интеллигибельный смысл жизни, изменяя привычную реальность. Смысл жизни, по определению, не так уж очевиден. Заблуждение, как принято считать, решительно принимает во внимание дедуктивный метод, <a href="#">tertium nоn datur</a>.</p>
                                </div>
                            </div>
                        </section>
                        <section>
                            <h2>Заголовок 2-ого уровня</h2>
                            <p>Весьма существенно следующее: аполлоновское начало мгновенно. Эротическое многопланово заканчивает резкий статус художника, подобный исследовательский подход к проблемам худ. типологии можно обнаружить у К.Фосслера. </p>
                        </section>
                        <section>
                            <h3>Заголовок 3-его уровня</h3>
                            <p>Переходное состояние просветляет незначительный постмодернизм, таким образом, сходные законы контрастирующего развития характерны и для процессов в психике. </p>
                            <p>Развивая эту тему, целое образа аккумулирует структурализм, подобный исследовательский подход к проблемам художественной типологии можно обнаружить у К.Фосслера. Эстетика трансформирует онтологический статус искусства, таким образом, сходные законы контрастирующего развития характерны и для процессов в психике. </p>
                        </section>
                        <section>
                            <h4>Заголовок 4-ого уровня</h4>
                            <ul class="mark_list left_half">
                                <li>Развивая эту тему</li>
                                <li>Целое образа аккумулирует</li>
                                <li>Исследовательский подход</li>
                                <li>Принцип мариартного нисхождения</li>
                            </ul>
                            <ol class="mark_num right_half">
                                <li>Развивая эту тему</li>
                                <li>Целое образа аккумулирует</li>
                                <li>Исследовательский подход</li>
                                <li>Принцип мариартного нисхождения</li>
                            </ol>
                        </section>
                        <section class="share">
                            <p>Понравилось? Поделитесь:</p>
                            <div class="share_links">
                                <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                                <div class="yashare-auto-init" data-yashareQuickServices="twitter,facebook,vkontakte,gplus,odnoklassniki" data-yashareTheme="counter" data-yashareType="medium"></div>
                            </div>
                        </section>
                        <footer>
                            <a href="#" class="page_prev">Предыдущая новость</a>
                            <a href="#" class="page_next">Следующая новость</a>
                        </footer>

                        <div class="comments_block">
                            <form action="" method="post" class="comments_form">
                                <h3>Ваш комментарий:</h3>

                                <fieldset>
                                    <textarea class="fld"></textarea>
                                </fieldset>

                                <fieldset>
                                    <button class="color_btn comment_add" type="submit"><span>Отправить</span></button>
                                </fieldset>
                            </form>
                            <div class="comment">
                                <div class="comment_title">
                                    <div class="comment_name">Вера Смирнова</div>
                                    <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                                </div>
                                <div class="comment_msg">
                                    <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                                    <a href="#" class="comment_answer_link">Ответить</a>
                                </div>
                            </div>
                            <div class="comment">
                                <div class="comment_title">
                                    <div class="comment_name">Вера Смирнова</div>
                                    <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                                </div>
                                <div class="comment_msg">
                                    <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                                    <a href="#" class="comment_answer_link">Ответить</a>
                                </div>
                                <div class="comment">
                                    <div class="comment_title">
                                        <div class="comment_name">Вера Смирнова</div>
                                        <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                                    </div>
                                    <div class="comment_msg">
                                        <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                                        <a href="#" class="comment_answer_link">Ответить</a>
                                    </div>
                                </div>
                            </div>
                            <div class="comment">
                                <div class="comment_title">
                                    <div class="comment_name">Вера Смирнова</div>
                                    <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                                </div>
                                <div class="comment_msg">
                                    <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                                    <a href="#" class="comment_answer_link">Ответить</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </section>
            </div>

        </div>
    </div>
</div>