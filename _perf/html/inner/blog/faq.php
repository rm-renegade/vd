<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <a href="blog.html">Блог</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Часто задаваемые вопросы</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Часто задаваемые вопросы</h2>
                <h4>Получите развернутый ответ на свой вопрос!</h4>
            </hgroup>

            <div class="wrapper">

                <aside class="aside">
                    <div class="sidebar_top_img">
                        <img src="/i/faq-pict.jpg" width="325" height="145" border="0">
                        <div class="faq_icon"></div>
                    </div>
                    <div class="sidebar shadow_medium">
                        <form class="faq_form sidebar_form" method="POST" action="">
                            <hgroup>
                                <h3>Задать свой вопрос</h3>
                            </hgroup>

                            <fieldset>
                                <input type="text" class="fld" value="Введите ваше имя" />
                            </fieldset>

                            <fieldset>
                                <input type="text" class="fld" value="Введите ваше e-mail" />
                            </fieldset>

                            <fieldset>
                                <textarea class="fld">Напишите ваш отзыв</textarea>
                            </fieldset>

                            <fieldset>
                                <p>Введите код с картинки:</p>
                                <div>
                                    <img class="captcha" src="/i/captcha.gif">
                                    <input type="text" class="fld fld_captcha_width" />
                                </div>
                            </fieldset>

                            <button type="submit" class="button sidebar_button"><span></span></button>
                        </form>
                    </div>

                </aside>

                <section class="section">
                    <div class="faq_list page">

                        <div class="faq_category">
                            <h2>Вопросы по покупке товара</h2>
                            <ul class="faq_subcategory">
                                <li class="faq_item">
                                    <span class="faq_question">Как добавлять товары в корзину?</span>
                                    <div class="faq_answer">
                                        <p>В интернет-магазине рядом с каждым товаром имеется кнопка "Купить". При однократном нажатии на нее в корзину добавляется 1 единица соответствующего товара. Под кнопкой "Купить" отображается уже имеющееся количество данного товара в корзине.</p>
                                        <p>Если количество единиц товара, которое Вы хотите купить небольшое, например 2-5 шт., то можно щелкнуть соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                                <li class="faq_item">
                                    <span class="faq_question">Как оформить заказ?</span>
                                    <div class="faq_answer">
                                        <p>В интернет-магазине рядом с каждым товаром имеется кнопка "Купить". При однократном нажатии на нее в корзину добавляется 1 единица соответствующего товара. Под кнопкой "Купить" отображается уже имеющееся количество данного товара в корзине.</p>
                                        <p>Если количество единиц товара, которое Вы хотите купить небольшое, например 2-5 шт., то можно щелкнуть соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                                <li class="faq_item">
                                    <span class="faq_question">Как внести поправки в заказ, если он уже оформлен?</span>
                                    <div class="faq_answer">
                                        <p>При однократном нажатии на нее в корзину добавляется 1 единица соответствующего товара. Под кнопкой "Купить" отображается уже имеющееся количество данного товара в корзине.</p>
                                        <p>Если количество единиц товара, которое Вы хотите купить небольшое, например 2-5 шт., то можно щелкнуть соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                                <li class="faq_item">
                                    <span class="faq_question">Как оплатить заказ?</span>
                                    <div class="faq_answer">
                                        <p>В интернет-магазине рядом с каждым товаром имеется кнопка "Купить". При однократном нажатии на нее в корзину добавляется 1 единица соответствующего товара. Под кнопкой "Купить" отображается уже имеющееся количество данного товара в корзине.</p>
                                        <p>2-5 шт., то можно щелкнуть соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="faq_category">
                            <h2>Вопросы по обучению</h2>
                            <ul class="faq_subcategory">
                                <li class="faq_item">
                                    <span class="faq_question">Как попасть на мастер-класс?</span>
                                    <div class="faq_answer">
                                        <p>В интернет-магазине рядом с каждым товаром имеется кнопка "Купить". При однократном нажатии на нее в корзину добавляется 1 единица соответствующего товара. Под кнопкой "Купить" отображается уже имеющееся количество данного товара в корзине.</p>
                                        <p>Если количество единиц товара, которое Вы хотите купить небольшое, например 2-5 шт., то можно щелкнуть соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                                <li class="faq_item">
                                    <span class="faq_question">Нужно ли покупать материалы?</span>
                                    <div class="faq_answer">
                                        <p>В интернет-магазине товара, которое Вы хотите купить небольшое, например 2-5 шт., то можно щелкнуть соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                                <li class="faq_item">
                                    <span class="faq_question">Проводите ли вы индивидуальные курсы?</span>
                                    <div class="faq_answer">
                                        <p>В интернет-магазине рядом с каждым товаром имеется кнопка "Купить". При однократном нажатии на нее в корзину добавляется 1 единица соответствующего товара. Под кнопкой "Купить" отображается уже имеющееся количество данного товара в корзине.</p>
                                        <p>Если количество единиц товара, которое Вы хотите купить небольшое, например 2-5 шт., то можно щелкнуть соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                                <li class="faq_item">
                                    <span class="faq_question">Есть ли у вас web-курсы по лепке?</span>
                                    <div class="faq_answer">
                                        <p>При однократном нажатии на нее в корзину добавляется 1 единица данного товара в корзине.</p>
                                        <p>Если количество единиц товара, которое Вы хотите купить соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                                <li class="faq_item">
                                    <span class="faq_question">Как происходит оплата?</span>
                                    <div class="faq_answer">
                                        <p>В интернет-магазине рядом с каждым товаром имеется кнопка "Купить". При однократном нажатии на нее в корзину добавляется 1 единица соответствующего товара. Под кнопкой "Купить" отображается уже имеющееся количество данного товара в корзине.</p>
                                        <p>Если количество единиц товара, которое Вы хотите купить небольшое, например 2-5 шт., то можно щелкнуть соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <div class="faq_category">
                            <h2>Вопросы по аккаунту</h2>
                            <ul class="faq_subcategory">
                                <li class="faq_item">
                                    <span class="faq_question">Не могу войти. Что делать?</span>
                                    <div class="faq_answer">
                                        <p>В интернет-магазине рядом с каждым товаром имеется кнопка "Купить". При однократном нажатии на нее в корзину добавляется 1 единица соответствующего товара. Под кнопкой "Купить" отображается уже имеющееся количество данного товара в корзине.</p>
                                        <p>Если количество единиц товара, которое Вы хотите купить небольшое, например 2-5 шт., то можно щелкнуть соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                                <li class="faq_item">
                                    <span class="faq_question">Не пришло письмо на e-mail для подтверждения аккаунта</span>
                                    <div class="faq_answer">
                                        <p>В интернет-магазине рядом с каждым товаром имеется кнопка "Купить".</p>
                                    </div>
                                </li>
                                <li class="faq_item">
                                    <span class="faq_question">Можно ли получить скидку?</span>
                                    <div class="faq_answer">
                                        <p>В интернет-магазине рядом с каждым товаром имеется кнопка "Купить". При однократном нажатии на нее в корзину добавляется 1 единица соответствующего товара. Под кнопкой "Купить" отображается уже имеющееся количество данного товара в корзине.</p>
                                        <p>Если количество единиц товара, которое Вы хотите купить небольшое, например 2-5 шт., то можно щелкнуть соответствующее количество раз по кнопке "Купить". Чтобы не нажимать большое количество раз по кнопке "Купить", предлагаем установить количество перед оформлением заказа в Вашей корзине. При этом сумма всего заказа и конкретного товара автоматически пересчитывается.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>



                    </div>
                </section>
            </div>
        </div>
    </div>
</div>