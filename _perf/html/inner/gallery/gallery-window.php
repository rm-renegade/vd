<link rel="stylesheet" href="/css/gallery.css"/>
<div class="gallery_overlay hidden"></div>
<div class="gallery_popup hidden">
    <div class="gallery_popup_window">
        <a class="gallery_popup_close" href="#"></a>

        <section class="slider">
            <p class="gallery_num-of-length">Фотография <span class="num"></span> из <span class="length"></span></p>
            <h3 class="gallery_title">Корзина с розами</h3>
            <div class="flexslider" id="gallery_slider">
                <ul class="slides">
                    <li><img src="/i/gallery-01-large.jpg" alt="" width="100%"/></li>
                    <li><img src="/i/galer-08.jpg" alt="" width="100%"/></li>
                    <li><img src="/i/galer-09.jpg" alt="" width="100%"/></li>
                    <li><img src="/i/galer-10.jpg" alt="" width="100%"/></li>
                </ul>
            </div>
        </section>

        <!-- jQuery -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <!-- script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script -->

        <!-- FlexSlider -->
        <script defer src="/js/jquery.flexslider-min.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#gallery_slider').flexslider({
                    slideshow: false,
                    animation: "fade",
                    directionNav: true,
                    controlNav: false,
                    prevText: "",
                    nextText: "",
                    start: function(slider){
                        var slides = $('#gallery_slider .slides').find('li');
                        var sliderLength = slides.length;

                        for(var i=0;i<sliderLength;i++){
                            if(slides[i].className == 'flex-active-slide'){
                                $('.gallery_num-of-length').find('.num').html(i+1);
                            }
                        }
                        $('.gallery_num-of-length').find('.length').html(sliderLength);
                    },
                    after: function(slider){
                        var slides = $('#gallery_slider .slides').find('li');
                        var sliderLength = slides.length;

                        for(var i=0;i<sliderLength;i++){
                            if(slides[i].className == 'flex-active-slide'){
                                $('.gallery_num-of-length').find('.num').html(i+1);
                            }
                        }
                        $('.gallery_num-of-length').find('.length').html(sliderLength);
                    }
                });

                $('.gallery_popup_close').click(function(){
                    $('.gallery_overlay').addClass('hidden');
                    $('.gallery_popup').addClass('hidden');

                    $('body').css('overflow-x','visible');
                })
            });
        </script>


        <div class="gallery_descr">
            <div class="gallery_left">
                <p class="gallery_descr_txt">Аналогия преобразует неоднозначный структурализм, tertium nоn datur. Исчисление предикатов поразительно. Гравитационный парадокс подчеркивает структурализм, не учитывая мнения авторитетов. </p>
                <div class="comments_block">

                    <!-- unlogin user -->
                    <div class="comments_attention">
                        Вы не можете оставить комментарии к данной публикации, так как вы не зарегистрированы на
                        сайте! <a href="/">Зарегистрироваться!</a>
                    </div>
                    <!-- login user -->
                    <form action="" method="post" class="comments_form">
                        <h3>Ваш комментарий:</h3>

                        <fieldset>
                            <textarea class="fld"></textarea>
                        </fieldset>

                        <fieldset>
                            <button class="color_btn comment_add" type="submit"><span>Отправить</span></button>
                        </fieldset>
                    </form>

                    <div class="comment">
                        <div class="comment_title">
                            <div class="comment_name">Вера Смирнова</div>
                            <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                        </div>
                        <div class="comment_msg">
                            <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                            <a href="#" class="comment_answer_link">Ответить</a>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comment_title">
                            <div class="comment_name">Вера Смирнова</div>
                            <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                        </div>
                        <div class="comment_msg">
                            <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                            <a href="#" class="comment_answer_link">Ответить</a>
                        </div>
                        <div class="comment">
                            <div class="comment_title">
                                <div class="comment_name">Вера Смирнова</div>
                                <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                            </div>
                            <div class="comment_msg">
                                <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                                <a href="#" class="comment_answer_link">Ответить</a>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comment_title">
                            <div class="comment_name">Вера Смирнова</div>
                            <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                        </div>
                        <div class="comment_msg">
                            <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                            <a href="#" class="comment_answer_link">Ответить</a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="gallery_right">
                <div class="gallery_sticker">
                    <p>Альбом: <br/>
                        <a href="#">Работы Оксаны Степановой</a>
                    </p>
                    <br/>
                    <p>Добавлено: <a href="#">12 августа, 2013</a></p>
                    <br/>
                    <p>Понравилось? Поделитесь:</p>
                    <div class="share_links">
                        <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                        <div class="yashare-auto-init" data-yashareQuickServices="twitter,facebook,vkontakte,gplus,odnoklassniki" data-yashareTheme="counter" data-yashareType="medium"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>