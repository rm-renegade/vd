<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="brick_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Галлерея</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser gallery_reduser">
            <hgroup>
                <h2 class="title_ornament">
                    <span>
                        <span>Галлерея</span>
                        <i class="flower"></i>
                    </span>
                </h2>
                <h4>Окунитесь в мир живого творчества</h4>
            </hgroup>

            <div class="wrapper">
                <div class="gallery_photolist">
                    <a class="gallery_photo" href="/i/gallery-01-large.jpg">
                        <img src="/i/galer-07.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-08.jpg">
                        <img src="/i/galer-08.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-09.jpg">
                        <img src="/i/galer-09.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-10.jpg">
                        <img src="/i/galer-10.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-11.jpg">
                        <img src="/i/galer-11.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-12.jpg">
                        <img src="/i/galer-12.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-13.jpg">
                        <img src="/i/galer-13.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-15.jpg">
                        <img src="/i/galer-15.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-16.jpg">
                        <img src="/i/galer-16.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-17.jpg">
                        <img src="/i/galer-17.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-18.jpg">
                        <img src="/i/galer-18.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-19.jpg">
                        <img src="/i/galer-19.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-20.jpg">
                        <img src="/i/galer-20.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-21.jpg">
                        <img src="/i/galer-21.jpg" width="209" height="196" alt=""/>
                    </a>
                    <a class="gallery_photo" href="/i/galer-22.jpg">
                        <img src="/i/galer-22.jpg" width="209" height="196" alt=""/>
                    </a>

                    <script type="text/javascript">
                        var photoIndex = 0;
                        $(document).ready(function(){
                            $('.gallery_photo').click(function(){

                                $('.gallery_overlay').removeClass('hidden');
                                $('.gallery_popup').removeClass('hidden');

                                $('body, html').scrollTop(0)
                                $('body').css('overflow-x','hidden');

                                return false;
                            })
                        })
                    </script>
                </div>
                <?php
                    BLOCK('paginator');
                ?>
            </div>
        </div>
    </div>
</div>