<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="brick_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Галлерея</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser gallery_reduser">
            <hgroup>
                <h2 class="title_ornament">
                    <span>
                        <span>Галлерея</span>
                        <i class="flower"></i>
                    </span>
                </h2>
                <h4>Окунитесь в мир живого творчества</h4>
            </hgroup>

            <div class="wrapper">
                <div class="columns">
                    <div class="column">
                        <article class="gallery_item">
                            <figure>
                                <a href="gallery-page.html">
                                    <img src="/i/gallery-01.jpg" width="276" height="179">
                                </a>
                                <span></span>
                            </figure>
                            <a href="gallery-page.html">Работы оксаны степановой</a>
                        </article>
                    </div>
                    <div class="column">
                        <article class="gallery_item">
                            <figure>
                                <a href="gallery-page.html">
                                    <img src="/i/gallery-02.jpg" width="276" height="179">
                                </a>
                                <span></span>
                            </figure>
                            <a href="gallery-page.html">Работы учеников</a>
                        </article>
                    </div>
                    <div class="column">
                        <article class="gallery_item">
                            <figure>
                                <a href="gallery-page.html">
                                    <img src="/i/gallery-03.jpg" width="276" height="179">
                                </a>
                                <span></span>
                            </figure>
                            <a href="gallery-page.html">Мыльная вечеринка</a>
                        </article>
                    </div>
                    <div class="column">
                        <article class="gallery_item">
                            <figure>
                                <a href="gallery-page.html">
                                    <img src="/i/gallery-04.jpg" width="276" height="179">
                                </a>
                                <span></span>
                            </figure>
                            <a href="gallery-page.html">Счастливые покупатели</a>
                        </article>
                    </div>
                    <div class="column">
                        <article class="gallery_item">
                            <figure>
                                <a href="gallery-page.html">
                                    <img src="/i/gallery-05.jpg" width="276" height="179">
                                </a>
                                <span></span>
                            </figure>
                            <a href="gallery-page.html">Выставка «В мире цветов»</a>
                        </article>
                    </div>
                    <div class="column">
                        <article class="gallery_item">
                            <figure>
                                <a href="gallery-page.html">
                                    <img src="/i/gallery-06.jpg" width="276" height="179">
                                </a>
                                <span></span>
                            </figure>
                            <a href="gallery-page.html">Выставка «Мастерская чудес»</a>
                        </article>
                    </div>
                </div>

                <?php
                    BLOCK('paginator');
                ?>
            </div>
        </div>
    </div>
</div>