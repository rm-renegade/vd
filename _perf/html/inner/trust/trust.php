<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Нам доверяют</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Нам доверяют</h2>
                <h4>Нам доверяют многие!</h4>
            </hgroup>

            <div class="news news_list">
                <div class="columns">
                    <article class="column article">
                        <a href="trust-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-07.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Гостиничный комплекс “КАРАВЕЛЛА”</p>
                        </a>
                        <em>г. Туапсе</em>
                    </article>
                    <article class="column article">
                        <a href="trust-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-08.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Ресторан русской кухни "Сани"</p>
                        </a>
                        <em>г. Туапсе</em>
                    </article>
                    <article class="column article">
                        <a href="trust-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-09.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Салон красоты "Сакура"</p>
                        </a>
                        <em>г. Краснодар</em>
                    </article>
                    <article class="column article">
                        <a href="trust-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-10.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Салон красоты "Сальери"</p>
                        </a>
                        <em>г. Туапсе</em>
                    </article>
                    <article class="column article">
                        <a href="trust-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-11.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Кубанский государственный университет</p>
                        </a>
                        <em>г. Краснодар</em>
                    </article>
                    <article class="column article">
                        <a href="trust-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-12.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Клиника «Акварель»</p>
                        </a>
                        <em>г. Краснодар</em>
                    </article>
                    <article class="column article">
                        <a href="trust-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-13.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Родильный дом №5</p>
                        </a>
                        <em>г. Краснодар</em>
                    </article>
                    <article class="column article">
                        <a href="trust-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-14.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Магазин "Ёжик вяжет"</p>
                        </a>
                        <em>г. Сочи</em>
                    </article>
                    <article class="column article">
                        <a href="trust-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-15.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>"Летучий Цвет" фотограф - Вита Кожевникова</p>
                        </a>
                        <em>г. Краснодар</em>
                    </article>
                    <article class="column article">
                        <a href="trust-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-16.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Цветы хищники</p>
                        </a>
                        <em>г. Москва</em>
                    </article>

                </div>
            </div>

        </div>
    </div>
</div>