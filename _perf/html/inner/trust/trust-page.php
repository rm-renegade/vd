<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <a href="trust.html">Нам доверяют</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Почему параллелен социальная психология искусства</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <div class="page">

                <aside class="aside">
                    <div class="sidebar shadow_medium">

                        <section class="sidebar_widget">
                            <header>Нам доверяют</header>
                            <?php
                                BLOCK('sidebar_slider');
                            ?>
                        </section>

                    </div>
                </aside>

                <section class="section">
                    <article class="page_content">
                        <hgroup>
                            <h1>Гостиничный комплекс «каравелла»</h1>
                            <h6>г. Туапсе</h6>
                            <a class="back" href="news.html">Вернуться</a>
                        </hgroup>
                        <section>
                            <div class="page_img_txt">
                                <div class="page_left">
                                    <div class="page_gallery_img">
                                        <img class="medium" src="/i/article-07.jpg" width="323" height="198" border="0">
                                    </div>
                                    <div class="page_gallery_reviews">
                                        <div class="g_img_left">
                                            <img class="small" src="/i/article-small-01.jpg" width="150" height="100">
                                        </div>
                                        <div class="g_img_right">
                                            <img class="small" src="/i/article-small-02.jpg" width="150" height="100">
                                        </div>
                                        <div class="g_img_left">
                                            <img class="small" src="/i/article-small-03.jpg" width="150" height="100">
                                        </div>
                                        <div class="g_img_right">
                                            <img class="small" src="/i/article-small-04.jpg" width="150" height="100">
                                        </div>
                                        <div class="g_img_left">
                                            <img class="small" src="/i/article-small-05.jpg" width="150" height="100">
                                        </div>
                                    </div>
                                </div>
                                <div class="page_right">
                                    <p>Априори, сомнение контролирует позитивизм, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.</p>
                                    <p>По своим философским взглядам <a href="#">Дезами был материалистом</a> и атеистом, последователем Гельвеция, однако гештальтпсихология иллюзорна.</p>
                                </div>
                            </div>
                        </section>
                        <section>

                        </section>
                        <footer>
                            <a href="#" class="page_prev">Предыдущая статья</a>
                            <a href="#" class="page_next">Следующая статья</a>
                        </footer>
                    </article>
                </section>

            </div>


        </div>

    </div>

</div>