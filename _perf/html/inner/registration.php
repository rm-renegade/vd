<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
       BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Регистрация</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Регистрация</h2>
                <h4>Создайте свой аккаунт!</h4>
            </hgroup>
            <div class="registration">
                <form method="post" action="" class="reg_form">

                    <p>* - поле обязательно для заполнения</p>

                    <fieldset>
                        <label>Фамилия*</label>
                        <div class="reg_form_right"><input type="text" class="fld fld_full_width" /></div>
                    </fieldset>

                    <fieldset>
                        <label>Имя*</label>
                        <div class="reg_form_right"><input type="text" class="fld fld_full_width" /></div>
                    </fieldset>

                    <fieldset>
                        <label>Телефон*</label>
                        <div class="reg_form_right">
                            <span>+7</span>
                            <input type="text" class="fld fld_code_width" />
                            <input type="text" class="fld fld_phone_width" />
                        </div>
                        <p class="example phone_example">например: 902 3564021</p>
                    </fieldset>

                    <fieldset>
                        <label>E-mail*</label>
                        <div class="reg_form_right"><input type="text" class="fld fld_full_width" /></div>
                    </fieldset>

                    <fieldset>
                        <label>Пароль*</label>
                        <div class="reg_form_right"><input type="text" class="fld fld_full_width" /></div>
                    </fieldset>

                    <fieldset class="reg_address_fieldset">
                        <p>Контактный<br> адрес:*</p>
                        <div class="reg_form_right">
                            <label>Регион / Область</label>
                            <select class="custom-select" name="region" id="region">
                                <option value="value1">Челябинская область</option>
                                <option value="value2">Московская область</option>
                                <option value="value3">Саратовская область</option>
                            </select>
                        </div>
                        <div class="reg_form_right">
                            <label>Город</label>
                            <select class="custom-select" name="city" id="city">
                                <option value="value1">Магнитогорск</option>
                                <option value="value2" selected="selected">Челябинск</option>
                                <option value="value3">Саратов</option>
                                <option value="value4">Санкт-Петербург</option>
                            </select>
                        </div>
                        <div class="reg_form_right">
                            <label>Адресс</label>
                            <input type="text" class="fld" />
                            <p class="example address_example">например: ул. Хрустальная, 23, под. 5, кв. 98</p>
                        </div>
                        <div class="reg_form_right">
                            <label>Индекс</label>
                            <input type="text" class="fld" />
                        </div>
                        <div class="reg_form_right">
                            <p class="inlay">Пожалуйста, выберите адрес доставки, тот же, что и контактный</p>
                        </div>
                        <div class="reg_form_right">
                            <p><a class="l_add add_address">Добавить новый адрес доставки</a></p>
                        </div>
                    </fieldset>
                    <fieldset>
                        <label>Введите код*</label>
                        <div class="reg_form_right">
                            <img class="captcha" src="/i/captcha.gif">
                            <input type="text" class="fld fld_captcha_width" />
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="reg_form_right">
                            <label class="checkbox">
                                <input type="checkbox" class="" />
                                <i>Я согласен на обработку моих персональных данных</i>
                            </label>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="reg_form_right">
                            <button type="submit" class="button reg_button">
                                <i></i>
                                <span></span>
                                <strong>Зарегистрироваться »</strong>
                            </button>
                        </div>
                    </fieldset>

                </form>
            </div>

        </div>
    </div>

</div>