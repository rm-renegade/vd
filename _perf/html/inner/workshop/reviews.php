<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>
    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="about.html">О мастерской</a>
                    <ul class="submenu">
                        <li><a href="about.html">О нас</a></li>
                        <li><a href="news.html">Новости</a></li>
                        <li class="curr"><span>Отзывы</span></li>
                        <li><a href="pressa.html">Пресса о мастерской</a></li>
                        <li><a href="#">Выставки</a></li>
                        <li><a href="#">Дипломы и награды</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Отзывы</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup class="hgroup">
                <h2 class="title_ornament">Отзывы</h2>
                <h4>Оставьте свое мнение о нашей мастерской!</h4>
            </hgroup>

            <div class="wrapper">

                <aside class="aside">
                    <div class="sidebar_top_img">
                        <img src="/i/reviews-pict.jpg" width="325" height="145" border="0">
                        <div class="reviews_icon"></div>
                    </div>
                    <div class="sidebar shadow_medium">
                        <form class="reviews_form sidebar_form" method="POST" action="">
                            <hgroup>
                                <h3>Написать отзыв</h3>
                            </hgroup>

                            <fieldset>
                                <input type="text" class="fld" value="Введите ваше имя" />
                            </fieldset>

                            <fieldset>
                                <input type="text" class="fld" value="Введите ваше e-mail" />
                            </fieldset>

                            <fieldset>
                                <textarea class="fld">Напишите ваш отзыв</textarea>
                            </fieldset>

                            <fieldset>
                                <p>Введите код с картинки:</p>
                                <div>
                                    <img class="captcha" src="/i/captcha.gif">
                                    <input type="text" class="fld fld_captcha_width" />
                                </div>
                            </fieldset>

                            <button type="submit" class="button sidebar_button">
                                <i></i>
                                <span></span>
                                <strong>Отправить отзыв</strong>
                            </button>
                        </form>
                    </div>

                </aside>

                <section class="section">
                    <article class="review_post shadow_center">
                        <div class="review_post_data">
                            <span class="left_half">Виктория Хмельнова</span>
                            <em class="right_half"><time datetime="2013-07-06" pubdate="">6 июня 2013</time> </em>
                        </div>
                        <p class="review_post_msg">Отличный ресурс и сервис. Все удобно упаковано. Большой плюс, что буклетики рецептов с фото: информативно  и понятно. Нам дома всем очень понравилось. Особенно десерт "Манго тапиока". Давно хотела попробовать, что за "зверь" такой эта тапиока и получила у Вас такую возможность. Обязательно закажем что-нибудь еще на пробу. Спасибо большое!</p>
                        <p class="review_post_cmnt"><strong>Оксана Степанова:</strong> спасибо большое за отзыв! Мне очень приятно)) Рада быть полезной) Желаю тебе успехов и продвижения как в творчестве так и в бизнесе!</p>
                    </article>
                    <article class="review_post shadow_center">
                        <div class="review_post_data">
                            <span class="left_half">Саша Карасенкова</span>
                            <em class="right_half"><time datetime="2013-07-06" pubdate="">6 июня 2013</time> </em>
                        </div>
                        <p class="review_post_msg">Спасибо за мастер-класс 3 июня. Мне действительно повезло, что я на нем была. Он сыграл важную роль в развитии меня как профессионального фотографа. Могу сказать, что унесла с собой багаж знаний которые очень помогут мне в будущем. Я разрешила для себя много вопросов о фотографии, обработке, о моментах общения с клиентом, психологии, о финансовой стороне фотографии. После мастер-класса я наконец поставила себе конкретную цель и знаю, что иду в правильном направлении. И огромное спасибо за такую позитивную, теплую, дружескую атмосферу, за терпение и детальные ответы на все вопросы. Я с радостью приду еще раз т.к. уверена, что обязательно будет что-то новое и всегда есть чему учиться! Для меня это оченб важно и ценно. Спасибо :))))))))</p>
                        <p class="review_post_cmnt"><strong>Оксана Степанова:</strong> Спасибо тебе за отзыв! Рада была быть полезной) И очень надеюсь, что все у тебя получится и ты будешь двигаться вперед и только вперед! Успехов!</p>
                    </article>
                    <article class="review_post shadow_center">
                        <div class="review_post_data">
                            <span class="left_half">Анастасия Проченко</span>
                            <em class="right_half"><time datetime="2013-07-06" pubdate="">6 июня 2013</time> </em>
                        </div>
                        <p class="review_post_msg">Спасибо вам огромное, вчера позвонила вам сообщила о том что получился отличный борщ, сегодня был тыквенный суп, только я еще добавила в него моркови, получилось отлично!!! Еще раз огромное спасибо!!!</p>
                        <p class="review_post_cmnt"><strong>Оксана Степанова:</strong> Всегда рада)) Спасибо!</p>
                    </article>
                    <article class="review_post shadow_center">
                        <div class="review_post_data">
                            <span class="left_half">Малика Соборовна</span>
                            <em class="right_half"><time datetime="2013-07-06" pubdate="">6 июня 2013</time> </em>
                        </div>
                        <p class="review_post_msg">Добрый день, Оксана! Благодарим Вас за чудесную работу, за память о прекрасных мгновениях в нашей жизни, запечатленных в кадре! Мне особенно по душе, что дочка с папой так здорово смотрятся вместе. Отдельные фото Аиши вызывают восторг, ее взгляд, выражение лица, такое сосредоточение в целом очень необычно. Дальнейших успехов Вам в работе, приятно было познакомиться, будем ждать новых встреч. Спасибо!</p>
                        <p class="review_post_cmnt"><strong>Оксана Степанова:</strong> Малика, спасибо огромное за добрые слова! До новых встреч!</p>
                    </article>

                    <?php
                        BLOCK('paginator');
                    ?>
                </section>

        </div>

    </div>

</div>