<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="about.html">О мастерской</a>
                    <ul class="submenu">
                        <li><a href="about.html">О нас</a></li>
                        <li class="curr"><span>Новости</span></li>
                        <li><a href="reviews.html">Отзывы</a></li>
                        <li><a href="pressa.html">Пресса о мастерской</a></li>
                        <li><a href="#">Выставки</a></li>
                        <li><a href="#">Дипломы и награды</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Новости</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Новости</h2>
                <h4>узнай о нас все самое интересное! </h4>
            </hgroup>

            <div class="news news_list">
                <div class="columns">
                    <article class="column article">
                        <a href="news-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-01.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Почему параллелен социальная психология искусства?</p>
                        </a>
                        <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
                    </article>
                    <article class="column article">
                        <a href="news-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-02.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Деструктивный экспрессионизм: эмпирическая история искусств?</p>
                        </a>
                        <em><time pubdate datetime="2013-07-06">2 января, 2013г.</time></em>
                    </article>
                    <article class="column article">
                        <a href="news-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-03.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Фактографический экзистенциализм: методология и особенности</p>
                        </a>
                        <em><time pubdate datetime="2013-07-06">26 сентября, 2012г.</time></em>
                    </article>
                    <article class="column article">
                        <a href="news-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-04.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Невротический текст — актуальная национальная задача</p>
                        </a>
                        <em><time pubdate datetime="2013-07-06">26 сентября, 2012г.</time></em>
                    </article>
                    <article class="column article">
                        <a href="news-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-05.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Почему интуитивно понятен миг?</p>
                        </a>
                        <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
                    </article>
                    <article class="column article">
                        <a href="news-page.html" class="article_link">
                            <div class="article_img">
                                <img src="/i/article-06.jpg">
                                <div class="overlap">Открыть статью</div>
                            </div>
                            <p>Глубокий психологический параллелизм в XXI веке</p>
                        </a>
                        <em><time pubdate datetime="2013-07-06">2 января, 2013г.</time></em>
                    </article>

                </div>
            </div>

            <?php
                BLOCK('paginator');
            ?>
        </div>
    </div>
</div>