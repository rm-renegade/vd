<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="brick_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="about.html">О мастерской</a>
                    <ul class="submenu">
                        <li class="curr"><span>О нас</span></li>
                        <li><a href="news.html">Новости</a></li>
                        <li><a href="reviews.html">Отзывы</a></li>
                        <li><a href="pressa.html">Пресса о мастерской</a></li>
                        <li><a href="#">Выставки</a></li>
                        <li><a href="#">Дипломы и награды</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>О нас</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">О нас</h2>
                <h4>Узнайте о нас много интересного!</h4>
            </hgroup>
        </div>

    </div>

    <div class="autor white_width">
        <div class="left_half">
            <div class="autor_txt table_cell">
                <h5>Здравствуйте!</h5>
                <p>Меня зовут Оксана Степанова, и я являюсь создателем и руководителем творческой мастерской “Вдохновение”</p>
                <p>Решила основать творческую мастерскую, в которой каждый взрослый и ребенок смог бы воплотить свои творческие мечты и фантазии в жизнь в атмосфере творчества.</p>
            </div>
        </div>
        <div class="right_half">
            <div class="autor_img table_cell">
                <img src="/i/autor-image.jpg" width="512" height="295">
            </div>
        </div>
    </div>

    <div class="brick_width">
        <div class="reduser">
            <div class="promo_workshop">

                <h3 class="title_strip_long">Почему именно творческая мастерская «Вдохновение» ?</h3>

                <div class="columns">
                    <div class="column">
                        <div class="promo_item">
                            <!-- todo: claccName -->
                            <div class="promo_icon tvorcheskaja"></div>
                            <p class="promo_descr">
                                <span>Творческая</span><br/>
                                потому, что каждый желающий может подобрать для себя любимый вид творчества, который откроет для него новые горизонты.
                            </p>
                        </div>
                    </div>
                    <div class="column">
                        <div class="promo_item">
                            <!-- todo: claccName -->
                            <div class="promo_icon workshop"></div>
                            <p class="promo_descr">
                                <span>Мастерская</span><br/>
                                потому, что мы мастерим, создаем, воплощаем в жизнь идеи в уютной душевной обстановке.
                            </p>
                        </div>
                    </div>
                    <div class="column">
                        <div class="promo_item">
                            <!-- todo: claccName -->
                            <div class="promo_icon vdohnovenie"></div>
                            <p class="promo_descr">
                                <span>Вдохновение</span><br/>
                                потому, что  это неиссякаемый источник жизненной силы, благодаря которому мир  становится ярче и насыщенней.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- todo: claccName -->
    <div class="white_width preimuschestva">
        <div class="reduser">

        <h3 class="title_strip_long">Почему именно мы?</h3>

        <div class="columns">
            <div class="column one">
                <!-- todo: claccName -->
                <div class="preimuschestva_title">Лучшие <span>преподаватели</span></div>
                <p class="preimuschestva_descr">В нашем коллективе работают исключительно люди преданные своему делу, интеллигентные, прошедшие обучение в своей области преподавания, которых объединяет желание развивать творческие возможности в себе и помогать в этом остальным.</p>
            </div>

            <div class="column">
                <div class="teacher">
                    <img src="/i/prepod-01.jpg" width="323" height="214">
                    <p><strong>Елена Шешеня</strong> - психолог</p>
                </div>

                <div class="teacher">
                    <img src="/i/prepod-03.jpg" width="323" height="214">
                    <p><strong>Алена Кеда</strong> - вышивка атласными лентами</p>
                </div>
            </div>

            <div class="column">
                <div class="teacher">
                    <img src="/i/prepod-02.jpg" width="323" height="214">
                    <p><strong>Бэла Таранова</strong> - декупаж, валяние, лепка FIMO, медведи Тедди</p>
                </div>

                <div class="teacher">
                    <img src="/i/prepod-04.jpg" width="323" height="214">
                    <p><strong>Гончарова Лидия</strong> - бисероплетение</p>
                </div>
            </div>
        </div>

        <div class="columns">
            <div class="aside two">
                <div class="preimuschestva_title">Уютная <span>атмосфера</span></div>
                <p class="preimuschestva_descr">Приятная творческая атмосфера, в которой приятно получать новые знания.</p>
            </div>

            <div class="section">
                <img src="/i/img-01.jpg" width="705" height="286">
            </div>
        </div>

        <div class="columns">
            <div class="aside three">
                <div class="preimuschestva_title"><span>Воплощение</span> идей</div>
                <p class="preimuschestva_descr">Люди, которые занимаются ручным ремеслом, получают возможность самореализоваться, ведь они могут воплотить идеи  и увидеть результат – это дает удивительное ощущение полета, гармонии.</p>
            </div>

            <div class="section">
                <img src="/i/img-02.jpg" width="705" height="286">
            </div>
        </div>

        <div class="more_title">А также...</div>

        <div class="columns">
            <div class="column">
                <div class="four">
                    <div class="preimuschestva_title"><span>Удобный</span> график</div>
                    <p class="preimuschestva_descr">Мы предложим на выбор групповые или индивидуальные занятия и возможность выбирать те дни и часы занятий, которые Вам удобны.</p>
                </div>
            </div>
            <div class="column">
                <div class="five">
                    <div class="preimuschestva_title"><span>Раскрытие</span> талантов</div>
                    <p class="preimuschestva_descr">На детских занятиях, прежде всего, обучаем детей умению творчески мыслить, развивать воображение. Помогаем каждому ребенку раскрыть свои творческие таланты и не огорчаться неудачам.</p>
                </div>
            </div>
            <div class="column">
                <div class="six">
                    <div class="preimuschestva_title"><span>Приятный</span> коллектив</div>
                    <p class="preimuschestva_descr">В конце обучения за чаепитием мы обсуждаем результаты в творчестве, делимся успехами, и конечно, фотосессия.</p>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="brick_width">
        <!-- todo: claccName -->
        <div class="workshop_end">

            <div class="more_title">И еще...</div>

            <p>Двери нашей творческой мастерской всегда открыты для учеников, окончивших обучение.  Мы рады видеть всех на наших встречах и праздниках. </p>
            <h5>Приходите, мы ждем именно Вас!</h5>

        </div>
    </div>
</div>