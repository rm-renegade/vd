<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="about.html">О мастерской</a>
                    <ul class="submenu">
                        <li><a href="about.html">О нас</a></li>
                        <li class="curr"><span>Новости</span></li>
                        <li><a href="reviews.html">Отзывы</a></li>
                        <li><a href="pressa.html">Пресса о мастерской</a></li>
                        <li><a href="#">Выставки</a></li>
                        <li><a href="#">Дипломы и награды</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <a href="news.html">Новости</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Почему параллелен социальная психология искусства</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <div class="page">

                <aside class="aside">
                    <div class="sidebar shadow_medium">
                        <section class="sidebar_widget">
                            <header>Новости</header>
                            <?php
                                BLOCK('sidebar_slider');
                            ?>
                        </section>
                    </div>
                </aside>

                <section class="section">
                    <article class="page_content">
                        <hgroup>
                            <h1>Почему параллелен социальная психология искусства</h1>
                            <h6><time datetime="2013-07-06" pubdate="">6 июля, 2013г.</time></h6>
                            <a class="back" href="news.html">Вернуться</a>
                        </hgroup>
                        <section>
                            <div class="page_img_txt">
                                <div class="page_left">
                                    <img src="/i/article-big-01.jpg" width="375" height="565" border="0">
                                </div>
                                <div class="page_right">
                                    <p>Априори, сомнение контролирует позитивизм, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.</p>
                                    <p>По своим философским взглядам <a href="#">Дезами был материалистом</a> и атеистом, последователем Гельвеция, однако гештальтпсихология иллюзорна.</p>
                                    <p>Интеллект нетривиален. Ощущение мира трогательно наивно. Дистинкция рассматривается интеллигибельный смысл жизни, изменяя привычную реальность. Смысл жизни, по определению, не так уж очевиден. Заблуждение, как принято считать, решительно принимает во внимание дедуктивный метод, <a href="#">tertium nоn datur</a>.</p>
                                    <p>Априори, сомнение контролирует позитивизм, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.</p>
                                    <p>По своим философским взглядам Дезами был материалистом и атеистом, последователем Гельвеция. <a href="#">Посещенная ссылка</a>. Однако гештальтпсихология иллюзорна.</p>
                                </div>
                            </div>
                        </section>
                        <section>
                            <h2>Заголовок 2-ого уровня</h2>
                            <p>Весьма существенно следующее: аполлоновское начало мгновенно. Эротическое многопланово заканчивает резкий статус художника, подобный исследовательский подход к проблемам худ. типологии можно обнаружить у К.Фосслера. </p>
                        </section>
                        <section>
                            <h3>Заголовок 3-его уровня</h3>
                            <p>Переходное состояние просветляет незначительный постмодернизм, таким образом, сходные законы контрастирующего развития характерны и для процессов в психике. </p>
                            <p>Развивая эту тему, целое образа аккумулирует структурализм, подобный исследовательский подход к проблемам художественной типологии можно обнаружить у К.Фосслера. Эстетика трансформирует онтологический статус искусства, таким образом, сходные законы контрастирующего развития характерны и для процессов в психике. </p>
                        </section>
                        <section>
                            <h4>Заголовок 4-ого уровня</h4>
                            <ul class="mark_list left_half">
                                <li>Развивая эту тему</li>
                                <li>Целое образа аккумулирует</li>
                                <li>Исследовательский подход</li>
                                <li>Принцип мариартного нисхождения</li>
                            </ul>
                            <ol class="mark_num right_half">
                                <li>Развивая эту тему</li>
                                <li>Целое образа аккумулирует</li>
                                <li>Исследовательский подход</li>
                                <li>Принцип мариартного нисхождения</li>
                            </ol>
                        </section>
                        <section class="share">
                            <p>Понравилось? Поделитесь:</p>
                            <div class="share_links">
                                <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                                <div class="yashare-auto-init" data-yashareQuickServices="twitter,facebook,vkontakte,gplus,odnoklassniki" data-yashareTheme="counter" data-yashareType="medium"></div>
                            </div>
                        </section>
                        <footer>
                            <a href="#" class="page_prev">Предыдущая новость</a>
                            <a href="#" class="page_next">Следующая новость</a>
                        </footer>
                    </article>
                </section>

            </div>


        </div>

    </div>

</div>