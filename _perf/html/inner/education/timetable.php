<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="brick_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="education.html">Обучение</a>
                    <ul class="submenu">
                        <li><a href="education.html">Курс обучения</a></li>
                        <li class="curr"><span>Расписание</span></li>
                        <li><a href="coupon.html">Подарочный купон</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Расписание</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup class="timetable_head">
                <h2 class="title_ornament">Расписание</h2>
                <h4>Выберите лучшее время для получения новых знаний!</h4>
            </hgroup>

            <div class="wrapper timetable">
                <table class="">
                    <tr>
                        <th class="name">Название курса</th>
                        <th class="day">Понедельник</th>
                        <th class="day">Вторник</th>
                        <th class="day">Среда</th>
                        <th class="day">Четверг</th>
                        <th class="day">Пятница</th>
                        <th class="day">Суббота</th>
                        <th class="day">Воскресенье</th>
                    </tr>
                    <tr>
                        <td class="name"><a href="#">Лепка CLAYCRAFT by DECO</a></td>
                        <td class="day"></td>
                        <td class="day">
                            <p>10.00 - 12.00</p>
                            <p>13.00 - 15.00</p>
                            <p>18.00 - 20.00</p>
                        </td>
                        <td class="day"></td>
                        <td class="day">
                            <p>10.00 - 12.00</p>
                            <p>13.00 - 15.00</p>
                            <p>18.00 - 20.00</p>
                        </td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                    </tr>
                    <tr>
                        <td class="name"><a href="#">Фоамиран</a></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"><p>С 11.00 </p></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                    </tr>
                    <tr>
                        <td class="name"><a href="#">Вышивка атласными лентами</a></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                    </tr>
                    <tr>
                        <td class="name"><a href="#">Бисероплетение</a></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"><p>С 11.00 </p></td>
                    </tr>
                    <tr>
                        <td class="name"><a href="#">Валяние (Фелтинг)</a></td>
                        <td class="day"><p>С 11.00 </p></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                    </tr>
                    <tr>
                        <td class="name"><a href="#">Лепка FIMO</a></td>
                        <td class="day"><p>С 11.00 </p></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                    </tr>
                    <tr>
                        <td class="name"><a href="#">Декупаж</a></td>
                        <td class="day"><p>С 11.00 </p></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                    </tr>
                    <tr>
                        <td class="name"><a href="#">Мыловарение</a></td>
                        <td class="day"><p>С 11.00 </p></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                    </tr>
                    <tr>
                        <td class="name"><a href="#">Детские группы</a></td>
                        <td class="day"><p>С 11.00 </p></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                        <td class="day"></td>
                    </tr>
                    <script>
                        // Table Stripy Background
                        $(document).ready(function(){
                            var rows = $('.timetable').find('tr');
                            for (var i = 0; i < rows.length; i++) // строки нумеруются с 0
                                rows[i].className += ((i % 2) == 0 ? " oddrows" : " evenrows");
                        })
                    </script>
                </table>
                <h4>Запишитесь по телефону  <span>+7 988 140 7002</span></h4>
            </div>
        </div>

    </div>
</div>
