<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="brick_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="education.html">Обучение</a>
                    <ul class="submenu">
                        <li class="curr"><span>Курс обучения</span></li>
                        <li><a href="timetable.html">Расписание</a></li>
                        <li><a href="coupon.html">Подарочный купон</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Курс обучения</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Курс обучения</h2>
                <h4>Научись создавать красоту своими руками! </h4>
            </hgroup>

            <div class="wrapper">
                <div class="columns">
                    <div class="column">
                        <article class="sticker shadow_medium">
                            <a href="/education/education-page.html" class="sticker_img">
                                <img src="/i/ob-01.jpg">
                                <span class="overlap">Открыть занятие</span>
                            </a>
                            <a href="/education/education-page.html" class="sticker_name">Лепка CLAYCRAFT by DECO</a>
                            <p>Продолжительность: <span>5-6 часов</span></p>
                            <p>Стоимость: <span>2400 руб</span></p>
                            <div class="more"><a href="/education/education-page.html">Узнать подробнее</a></div>
                        </article>
                    </div>
                    <div class="column">
                        <article class="sticker shadow_medium">
                            <a href="/education/education-page.html" class="sticker_img">
                                <img src="/i/ob-02.jpg">
                                <span class="overlap">Открыть занятие</span>
                            </a>
                            <a href="/education/education-page.html" class="sticker_name">Лепка CLAYCRAFT by DECO</a>
                            <p>Продолжительность: <span>5-6 часов</span></p>
                            <p>Стоимость: <span>2400 руб</span></p>
                            <div class="more"><a href="/education/education-page.html">Узнать подробнее</a></div>
                        </article>
                    </div>
                    <div class="column">
                        <article class="sticker shadow_medium">
                            <a href="/education/education-page.html" class="sticker_img">
                                <img src="/i/ob-03.jpg">
                                <span class="overlap">Открыть занятие</span>
                            </a>
                            <a href="/education/education-page.html" class="sticker_name">Лепка CLAYCRAFT by DECO</a>
                            <p>Продолжительность: <span>5-6 часов</span></p>
                            <p>Стоимость: <span>2400 руб</span></p>
                            <div class="more"><a href="/education/education-page.html">Узнать подробнее</a></div>
                        </article>
                    </div>

                    <div class="column">
                        <article class="sticker shadow_medium">
                            <a href="/education/education-page.html" class="sticker_img">
                                <img src="/i/ob-04.jpg">
                                <span class="overlap">Открыть занятие</span>
                            </a>
                            <a href="/education/education-page.html" class="sticker_name">Лепка CLAYCRAFT by DECO</a>
                            <p>Продолжительность: <span>5-6 часов</span></p>
                            <p>Стоимость: <span>2400 руб</span></p>
                            <div class="more"><a href="/education/education-page.html">Узнать подробнее</a></div>
                        </article>
                    </div>
                    <div class="column">
                        <article class="sticker shadow_medium">
                            <a href="/education/education-page.html" class="sticker_img">
                                <img src="/i/ob-05.jpg">
                                <span class="overlap">Открыть занятие</span>
                            </a>
                            <a href="/education/education-page.html" class="sticker_name">Лепка CLAYCRAFT by DECO</a>
                            <p>Продолжительность: <span>5-6 часов</span></p>
                            <p>Стоимость: <span>2400 руб</span></p>
                            <div class="more"><a href="/education/education-page.html">Узнать подробнее</a></div>
                        </article>
                    </div>
                    <div class="column">
                        <article class="sticker shadow_medium">
                            <a href="/education/education-page.html" class="sticker_img">
                                <img src="/i/ob-06.jpg">
                                <span class="overlap">Открыть занятие</span>
                            </a>
                            <a href="/education/education-page.html" class="sticker_name">Лепка CLAYCRAFT by DECO</a>
                            <p>Продолжительность: <span>5-6 часов</span></p>
                            <p>Стоимость: <span>2400 руб</span></p>
                            <div class="more"><a href="/education/education-page.html">Узнать подробнее</a></div>
                        </article>
                    </div>

                </div>

                <?php
                    BLOCK('paginator');
                ?>
            </div>

        </div>

    </div>

</div>