<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="brick_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="education.html">Обучение</a>
                    <ul class="submenu">
                        <li class="curr"><span>Курс обучения</span></li>
                        <li><a href="timetable.html">Расписание</a></li>
                        <li><a href="coupon.html">Подарочный купон</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="education.html">Курс обучения</a>
                    <ul class="submenu">
                        <li class="curr"><span>Вышивка атласными лентами</span></li>
                        <li><a href="education-page.html">Вышивка атласными лентами</a></li>
                        <li><a href="education-page.html">Вышивка атласными лентами</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Вышивка атласными лентами</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Вышивка атласными лентами</h2>
                <h4>Научись создавать красоту своими руками! </h4>
            </hgroup>

            <div class="wrapper">
                <aside class="aside">
                    <div class="sidebar sidebar_seminar">
                        <section class="seminar_sticker">
                            <p>&nbsp;&nbsp;<i>&nbsp;&nbsp;*&nbsp;&nbsp;-&nbsp;&nbsp;расчет за каждое занятие</i></p>

                            <div class="seminar_sticker_item">
                                <i class="duration"></i>
                                <div>
                                    <p>Продолжительность: *</p>
                                    <span>2 часа 30 минут</span>
                                </div>
                            </div>

                            <div class="seminar_sticker_item">
                                <i class="price"></i>
                                <div>
                                    <p>Стоимость: *</p>
                                    <span>400 руб.</span>
                                </div>
                            </div>

                            <div class="seminar_sticker_item">
                                <i class="materials"></i>
                                <div>
                                    <p>Материалы</p>
                                    <span>предоставляются</span>
                                </div>
                            </div>

                        </section>
                    </div>
                </aside>
                <section class="section">
                    <div class="section_slider flexslider" id="pict_slider">
                        <ul class="slides">
                            <li>
                                <article class="article">
                                    <a href="#" class="article_link">
                                        <figure>
                                            <img src="/i/slide-01.jpg" width="700" height="365">
                                        </figure>
                                    </a>
                                </article>
                            </li>
                            <li>
                                <article class="article">
                                    <a href="#" class="article_link">
                                        <figure>
                                            <img src="/i/slide-02.jpg" width="700" height="365">
                                        </figure>
                                    </a>
                                </article>
                            </li>
                            <li>
                                <article class="article">
                                    <a href="#" class="article_link">
                                        <figure>
                                            <img src="/i/slide-03.jpg" width="700" height="365">
                                        </figure>
                                    </a>
                                </article>
                            </li>
                        </ul>
                    </div>
                    <!-- FlexSlider -->
                    <script defer src="/js/jquery.flexslider-min.js"></script>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $('#pict_slider').flexslider({
                                animation: "fade",
                                easing: "swing",
                                directionNav: false
                            });
                        });
                    </script>
                </section>
            </div>
            <div class="wrapper">
                <aside class="aside">
                    <button class="button two_stroke_button" type="submit">
                        <strong>Я хочу пойти</strong>
                        <i>или задать интересующие вопросы »</i>
                        <span></span>
                    </button>
                </aside>
                <section class="section">
                    <div class="wrapper teacher_wrapper">
                        <figure>
                            <img src="/i/prepod-03-medium.jpg" width="352" height="232">
                        </figure>
                        <div>
                            <p class="name">Преподаватель - <strong>Алена Кеда</strong></p>
                            <p>Для меня вышивка лентами - источник гордости и удовольствия, а мои успехи приносят радость мне и моим близким.</p>
                            <p>С   удовольствием поделюсь с вами своими секретами, маленькими хитростями,  опытом и всем,  что я узнала и продолжаю узнавать о вышивке  лентами.</p>
                        </div>
                    </div>

                    <section class="course_descr">
                        <h5>Курс - Вышивка атласными лентами</h5>
                        <p>Каждый желающий может  освоить эту прекрасную технику, и поверьте, вы будете удивлены,    какие   изумительные  работы   создадите своими руками. Индивидуальный подход к каждому ученику позволит добиться  желаемых результатов, при этом в полной мере насладитесь  процессом.</p>
                        <p>На занятия приглашаются ученики с любым уровнем подготовки. На уроке мы осваиваем основные  ленточные  стежки, подробно разбираем один  цветок. На каждом занятии вы сделаете небольшую  мини-композицию.</p>

                        <ol class="mark_num left">
                            <li>Нарцисс,  разнотравие</li>
                            <li>Ромашки, фиалки</li>
                            <li>Глициния</li>
                            <li>Роза вышитая</li>
                        </ol>
                        <ol class="mark_num left" start="5">
                            <li>Роза сборная</li>
                            <li>Сирень</li>
                            <li>Ландыш</li>
                            <li>Пион сборный</li>
                        </ol>
                        <ol class="mark_num left" start="9">
                            <li>Пион вышитый</li>
                            <li>Мак</li>
                            <li>Клевер, васильки</li>
                        </ol>

                    </section>
                    <button class="button long_button" type="submit">
                        <strong>Я хочу пойти на этот мастер-класс</strong>
                        <i>или задать интересующие вопросы »</i>
                        <span></span>
                    </button>
                    <section class="share">
                        <p>Понравилось? Поделитесь:</p>
                        <div class="share_links">
                            <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                            <div class="yashare-auto-init" data-yashareQuickServices="twitter,facebook,vkontakte,gplus,odnoklassniki" data-yashareTheme="counter" data-yashareType="medium"></div>
                        </div>
                    </section>
                </section>
            </div>

        </div>
    </div>
</div>