<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="brick_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="education.html">Обучение</a>
                    <ul class="submenu">
                        <li><a href="education.html">Курс обучения</a></li>
                        <li><a href="timetable.html">Расписание</a></li>
                        <li class="curr"><span>Подарочный купон</span></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Подарочный купон</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Подарочный купон</h2>
                <h4>Сделайте приятное близким!</h4>
            </hgroup>

            <div class="coupons wrapper title_strip_long">
                <div class="coupon_head">
                    <div class="table_cell">
                        <h3>Сертификаты, чтобы преобрести  материал или готовое изделие <br/> - украшение или композицию</h3>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <article class="coupon">
                            <figure class="shadow_medium"><img src="/i/ob-sert-01.jpg"></figure>
                            <div class="coupon_action"><a class="color_btn coupon_buy" href="#"><span>Купить</span></a></div>
                        </article>
                    </div>
                    <div class="column">
                        <article class="coupon">
                            <figure class="shadow_medium"><img src="/i/ob-sert-02.jpg"></figure>
                            <div class="coupon_action"><a class="color_btn coupon_buy" href="#"><span>Купить</span></a></div>
                        </article>
                    </div>
                    <div class="column">
                        <article class="coupon">
                            <figure class="shadow_medium"><img src="/i/ob-sert-03.jpg"></figure>
                            <div class="coupon_action"><a class="color_btn coupon_buy" href="#"><span>Купить</span></a></div>
                        </article>
                    </div>
                </div>

                <div class="coupon_head">
                    <div class="table_cell">
                        <h3>Сертификаты на участие в мастер-классах</h3>
                    </div>
                </div>
                <div class="columns">
                    <div class="column">
                        <article class="coupon">
                            <h3>Clay Craft by DECO - 350 руб.</h3>
                            <figure class="shadow_medium"><img src="/i/ob-sert-04.jpg"></figure>
                            <div class="coupon_action"><a class="color_btn coupon_buy" href="#"><span>Купить</span></a></div>
                        </article>
                    </div>
                    <div class="column">
                        <article class="coupon">
                            <h3>Clay Craft by DECO - 1000 руб.</h3>
                            <figure class="shadow_medium"><img src="/i/ob-sert-05.jpg"></figure>
                            <div class="coupon_action"><a class="color_btn coupon_buy" href="#"><span>Купить</span></a></div>
                        </article>
                    </div>
                    <div class="column">
                        <article class="coupon">
                            <h3>фОАМИРАН - 450 рУБ.</h3>
                            <figure class="shadow_medium"><img src="/i/ob-sert-06.jpg"></figure>
                            <div class="coupon_action"><a class="color_btn coupon_buy" href="#"><span>Купить</span></a></div>
                        </article>
                    </div>
                </div>
            </div>

            <div class="coupon_rules wrapper">
                <div class="aside">
                    <h3>сертификат можно использовать:</h3>
                    <ul class="mark_list">
                        <li>совершая покупку у нас в офисе</li>
                        <li>для оплаты занятий и материалов</li>
                        <li>для покупки украшений и композиций</li>
                    </ul>
                </div>
                <div class="section">
                    <h3>ПРАВИЛА ПОЛЬЗОВАНИЯ СЕРТИФИКАТОМ:</h3>
                    <ul class="mark_list">
                        <li>Настоящий сертификат дает право держателю на преобретение любого украшения, композиции или посещения мастер-класса по выбранному направлению.</li>
                        <li>Один человек может использовать только один сертификат, вы можете передать сертификат вашим друзьям и близким.</li>
                        <li>Срок действия всех сертификатов: 2 месяца (включительно).</li>
                        <li>Подарочный сертификат не подлежит  возврату и обмену на денежные средства.</li>
                        <li>Необходима предварительная запись на мастер-классы по телефону +8 (988) 140-70-02</li>
                        <li>Обязательно предъявляйте сертификат при посещении мастер-класса или преобретения вашей покупки.</li>
                    </ul>
                </div>
            </div>

        </div>

    </div>

</div>