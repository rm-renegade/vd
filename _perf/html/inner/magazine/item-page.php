<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="about.html">Магазин</a>
                    <ul class="submenu">
                        <li class="curr"><a href="#">Композиции</a></li>
                        <li><a href="#">Глина</a></li>
                        <li><a href="#">Инструменты</a></li>
                        <li><a href="#">Книги</a></li>
                        <li><a href="#">Аксессуары</a></li>
                        <li><a href="#">ФОМ ЭВА (фоамиран)</a></li>
                        <li><a href="#">Заготовки из пенопласта</a></li>
                        <li><a href="#">Фурнитура</a></li>
                        <li><a href="#">Декор</a></li>
                        <li><a href="#">Кашпо, корзины, вазы</a></li>
                        <li><a href="#">Лента декоративная</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="about.html">Работы Оксаны Степановой</a>
                    <ul class="submenu">
                        <li class="curr"><a href="#">Композиции</a></li>
                        <li><a href="#">Свадебные аксессуары</a></li>
                        <li><a href="#">Украшения</a></li>
                        <li><a href="#">Куклы</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <a href="#">Композиции</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Букет «Нежный»</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <div class="page">
                <aside class="aside">
                    <div class="sidebar shadow_medium">
                        <section class="sidebar_widget">
                            <header>Магазин</header>
                            <nav class="sidebar_menu">
                                <ul class="sidebar_menu_mark_list">
                                    <li class="submenulink curr">
                                        <span>Работы Оксаны степановой</span>
                                        <ul class="submenu">
                                            <li class="curr">
                                                <span>композиции</span>
                                            </li>
                                            <li>
                                                <a href="#">свадебные аксессуары</a>
                                            </li>
                                            <li>
                                                <a href="#">украшения</a>
                                            </li>
                                            <li>
                                                <a href="#">куклы</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Глина</a>
                                    </li>
                                    <li>
                                        <a href="#">Инструменты</a>
                                    </li>
                                    <li>
                                        <a href="#">Книги</a>
                                    </li>
                                    <li>
                                        <a href="#">Аксессуары</a>
                                    </li>
                                    <li>
                                        <a href="#">ФОМ ЭВА (фоамиран)</a>
                                    </li>
                                    <li>
                                        <a href="#">Заготовки из пенопласта</a>
                                    </li>
                                    <li>
                                        <a href="#">Фурнитура</a>
                                    </li>
                                    <li>
                                        <a href="#">Декор</a>
                                    </li>
                                    <li>
                                        <a href="#">Кашпо, корзины, вазы</a>
                                    </li>
                                    <li>
                                        <a href="#">Лента декоративная</a>
                                    </li>
                                </ul>
                            </nav>
                        </section>
                    </div>

                    <a class="button delivery_button" href="item-delivery.html">
                        <strong>Доставка и оплата</strong>
                        <i>узнать больше »</i>
                        <span></span>
                    </a>

                    <a class="round_link_pink_aside" href="#"></a>

                </aside>
                <section class="section">
                    <article class="page_content">
                        <hgroup>
                            <h1>Букет «Нежный»</h1>
                            <a class="back" href="news.html">Вернуться</a>
                        </hgroup>
                        <section class="item_descr">
                            <div class="page_img_txt">
                                <div class="page_left">

                                    <div class="item_gallery shadow_medium">
                                        <div class="item_gallery_main flexslider" id="item_gallery_main">
                                            <ul class="item_gallery_main_list slides">
                                                <li>
                                                    <figure>
                                                        <img src="/i/magaz-01.jpg" alt=""/>
                                                    </figure>
                                                </li>
                                                <li>
                                                    <figure>
                                                        <img src="/i/magaz-02.jpg" alt=""/>
                                                    </figure>
                                                </li>
                                                <li>
                                                    <figure>
                                                        <img src="/i/magaz-03.jpg" alt=""/>
                                                    </figure>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="item_gallery_previews flexslider" id="item_gallery_previews">
                                            <ul class="item_gallery_previews_list slides">
                                                <li>
                                                    <figure>
                                                        <img src="/i/magaz-01.jpg" alt=""/>
                                                    </figure>
                                                </li>
                                                <li>
                                                    <figure>
                                                        <img src="/i/magaz-02.jpg" alt=""/>
                                                    </figure>
                                                </li>
                                                <li>
                                                    <figure>
                                                        <img src="/i/magaz-03.jpg" alt=""/>
                                                    </figure>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- FlexSlider -->
                                    <script defer src="/js/jquery.flexslider-min.js"></script>
                                    <script type="text/javascript">
                                        $(document).ready(function(){
                                            // The slider being synced must be initialized first
                                            $('#item_gallery_previews').flexslider({
                                                animation: "slide",
                                                controlNav: false,
                                                directionNav: false,
                                                animationLoop: false,
                                                slideshow: false,
                                                itemWidth: 103,
                                                asNavFor: '#item_gallery_main'
                                            });
                                            $('#item_gallery_main').flexslider({
                                                animation: "slide",
                                                controlNav: false,
                                                directionNav: false,
                                                animationLoop: false,
                                                slideshow: false,
                                                itemWidth: 317,
                                                sync: "#item_gallery_previews"
                                            });
                                        });
                                    </script>

                                </div>
                                <div class="page_right">
                                    <p>
                                        <strong>Состав:</strong> розы, орхидеи, гортензии<br />
                                        <strong>Размер:</strong> 25 х 4<br />
                                        <strong>Материал:</strong> пластик
                                    </p>
                                    <p>Априори, сомнение контролирует позитивизм, однако Зигварт считал критерием истинности необходимость и общезначимость, для которых нет никакой опоры в объективном мире.</p>
                                    <p>По своим философским взглядам <a href="#">Дезами был материалистом</a> и атеистом, последователем Гельвеция, однако гештальтпсихология.</p>

                                    <div class="action">
                                        <div class="left_half">
                                            <span>под заказ</span>
                                            <p>2200 руб</p>
                                        </div>
                                        <div class="right_half">
                                            <a class="color_btn coupon_buy" href="#"><span>Купить</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section>
                            <p>Интеллект нетривиален. Ощущение мира трогательно наивно. Дистинкция рассматривается интеллигибельный смысл жизни, изменяя привычную реальность. Смысл жизни, по определению, не так уж очевиден. Заблуждение, как принято считать, решительно принимает во внимание дедуктивный метод, tertium nоn datur.</p>
                        </section>

                        <section class="share">
                            <p>Понравилось? Поделитесь:</p>
                            <div class="share_links">
                                <script type="text/javascript" src="//yandex.st/share/share.js" charset="utf-8"></script>
                                <div class="yashare-auto-init" data-yashareQuickServices="twitter,facebook,vkontakte,gplus,odnoklassniki" data-yashareTheme="counter" data-yashareType="medium"></div>
                            </div>
                        </section>

                        <footer>
                            <a href="#" class="page_prev">Предыдущий товар</a>
                            <a href="#" class="page_next">Следующий товар</a>
                        </footer>

                        <section class="catalog">
                            <hgroup>
                                <h1>Похожие товары</h1>
                            </hgroup>
                            <div class="columns_small catalog">
                                <div class="column_small">
                                    <article class="item">
                                        <a href="/magazine/item-page.html" class="item_img">
                                            <img src="/i/magaz-01.jpg">
                                            <!-- span class="overlap">Открыть занятие</span -->
                                        </a>
                                        <a href="/magazine/item-page.html" class="item_name">Букет «Нежный»</a>

                                        <div class="action">
                                            <div class="left_half">
                                                <span>в наличии</span>
                                                <p>5500 руб</p>
                                            </div>
                                            <div class="right_half">
                                                <a class="color_btn coupon_buy" href="#"><span>Купить</span></a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="column_small">
                                    <article class="item">
                                        <a href="/magazine/item-page.html" class="item_img">
                                            <img src="/i/magaz-02.jpg">
                                            <!-- span class="overlap">Открыть занятие</span -->
                                        </a>
                                        <a href="/magazine/item-page.html" class="item_name">Букет «Жемчужные розы»</a>

                                        <div class="action">
                                            <div class="left_half">
                                                <span>под заказ</span>
                                                <p>2200 руб</p>
                                            </div>
                                            <div class="right_half">
                                                <a class="color_btn coupon_buy" href="#"><span>Заказать</span></a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                                <div class="column_small">
                                    <article class="item">
                                        <a href="/magazine/item-page.html" class="item_img">
                                            <img src="/i/magaz-03.jpg">
                                            <!-- span class="overlap">Открыть занятие</span -->
                                        </a>
                                        <a href="/magazine/item-page.html" class="item_name">Букет «Жемчужные розы»</a>

                                        <div class="action">
                                            <div class="left_half">
                                                <span>под заказ</span>
                                                <p>2200 руб</p>
                                            </div>
                                            <div class="right_half">
                                                <a class="color_btn coupon_buy" href="#"><span>Заказать</span></a>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        </section>

                        <div class="comments_block">

                            <!-- unlogin user -->
                            <div class="comments_attention">
                                Вы не можете оставить комментарии к данной публикации, так как вы не зарегистрированы на
                                сайте! <a href="/">Зарегистрироваться!</a>
                            </div>
                            <!-- login user -->
                            <form action="" method="post" class="comments_form">
                                <h3>Ваш комментарий:</h3>

                                <fieldset>
                                    <textarea class="fld"></textarea>
                                </fieldset>

                                <fieldset>
                                    <button class="color_btn comment_add" type="submit"><span>Отправить</span></button>
                                </fieldset>
                            </form>
                            <div class="comment">
                                <div class="comment_title">
                                    <div class="comment_name">Вера Смирнова</div>
                                    <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                                </div>
                                <div class="comment_msg">
                                    <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                                    <a href="#" class="comment_answer_link">Ответить</a>
                                </div>
                            </div>
                            <div class="comment">
                                <div class="comment_title">
                                    <div class="comment_name">Вера Смирнова</div>
                                    <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                                </div>
                                <div class="comment_msg">
                                    <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                                    <a href="#" class="comment_answer_link">Ответить</a>
                                </div>
                                <div class="comment">
                                    <div class="comment_title">
                                        <div class="comment_name">Вера Смирнова</div>
                                        <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                                    </div>
                                    <div class="comment_msg">
                                        <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                                        <a href="#" class="comment_answer_link">Ответить</a>
                                    </div>
                                </div>
                            </div>
                            <div class="comment">
                                <div class="comment_title">
                                    <div class="comment_name">Вера Смирнова</div>
                                    <div class="comment_date"><time datetime="" pubdate="">02.08.2013</time></div>
                                </div>
                                <div class="comment_msg">
                                    <p>Дедуктивный метод выводит типичный здравый смысл, отрицая очевидное. Позитивизм, конечно, нетривиален. Концепция понимает под собой принцип восприятия, не учитывая мнения авторитетов.</p>
                                    <a href="#" class="comment_answer_link">Ответить</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </section>
            </div>

        </div>

    </div>

</div>