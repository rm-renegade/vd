<link rel="stylesheet" href="/css/gallery.css"/>
<div class="gallery_overlay"></div>

<div class="mini_popup">
    <div class="mini_popup_window">
        <a href="#" class="gallery_popup_close"></a>
        <!-- FORMA -->
        <div class="login_form">
            <fieldset>
                <h5>
                    <span> <i class="success_icon"></i>
                        Вы совершили покупку</span>
                </h5>

                <p>
                    <em>Спасибо за заказ! Дополнительная</br> информация будет направлена на ваш электронный адрес.</em>
                </p>
            </fieldset>
        </div>
        <!-- FORMA END -->
    </div>
</div>