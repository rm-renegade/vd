<?php
/* Содержание сайта */
?>
<div id="conteiner">
<?php
BLOCK('menu');
?>

<div class="white_width">

    <div class="breadcrumbs">
        <p>Вы здесь:</p>
        <ul>
            <li>
                <a href="/">Главная</a>
            </li>
            <li class="breadcrumbs_separator">|</li>
            <li class="breadcrumbs_submenulink">
                <a href="/magazine/catalog.html">Магазин</a>
                <ul class="submenu">
                    <li class="curr"><a href="#">Композиции</a></li>
                    <li><a href="#">Глина</a></li>
                    <li><a href="#">Инструменты</a></li>
                    <li><a href="#">Книги</a></li>
                    <li><a href="#">Аксессуары</a></li>
                    <li><a href="#">ФОМ ЭВА (фоамиран)</a></li>
                    <li><a href="#">Заготовки из пенопласта</a></li>
                    <li><a href="#">Фурнитура</a></li>
                    <li><a href="#">Декор</a></li>
                    <li><a href="#">Кашпо, корзины, вазы</a></li>
                    <li><a href="#">Лента декоративная</a></li>
                </ul>
            </li>
            <li class="breadcrumbs_separator">|</li>
            <li>
                <a href="/magazine/cart.html">Моя корзина</a>
            </li>
            <li class="breadcrumbs_separator">|</li>
            <li>
                <span>Форма заказа</span>
            </li>
        </ul>

        <div class="clear"></div>
    </div>

    <div class="clear"></div>

    <div class="reduser">
        <div class="page">
            <aside class="aside">
                <div class="sidebar shadow_medium">
                    <section class="sidebar_widget">
                        <header>Магазин</header>
                        <nav class="sidebar_menu">
                            <ul class="sidebar_menu_mark_list">
                                <li class="submenulink curr">
                                    <span>Работы Оксаны степановой</span>
                                    <ul class="submenu">
                                        <li class="curr">
                                            <span>композиции</span>
                                        </li>
                                        <li>
                                            <a href="#">свадебные аксессуары</a>
                                        </li>
                                        <li>
                                            <a href="#">украшения</a>
                                        </li>
                                        <li>
                                            <a href="#">куклы</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#">Глина</a>
                                </li>
                                <li>
                                    <a href="#">Инструменты</a>
                                </li>
                                <li>
                                    <a href="#">Книги</a>
                                </li>
                                <li>
                                    <a href="#">Аксессуары</a>
                                </li>
                                <li>
                                    <a href="#">ФОМ ЭВА (фоамиран)</a>
                                </li>
                                <li>
                                    <a href="#">Заготовки из пенопласта</a>
                                </li>
                                <li>
                                    <a href="#">Фурнитура</a>
                                </li>
                                <li>
                                    <a href="#">Декор</a>
                                </li>
                                <li>
                                    <a href="#">Кашпо, корзины, вазы</a>
                                </li>
                                <li>
                                    <a href="#">Лента декоративная</a>
                                </li>
                            </ul>
                        </nav>
                    </section>
                </div>

                <a class="button delivery_button" href="item-delivery.html">
                    <strong>Доставка и оплата</strong>
                    <i>узнать больше »</i>
                    <span></span>
                </a>

                <a class="round_link_pink_aside" href="#"></a>
            </aside>

            <section class="section">
                <article class="page_content">
                    <hgroup>
                        <h1>Форма заказа</h1>
                        <h6 class="subtitle">В корзине <span class="cartTotalNum">3 товара</span> <br/>на сумму <span class="cartTotalSum">4 800</span> руб.</h6>
                        <a class="back" href="/magazine/cart.html">Вернуться</a>
                    </hgroup>

                    <section class="cart_table">
                        <form id="cartOrderForm" class="order_form">

                            <div class="cart_order_steps">
                                <div class="cart_order_steps_strip"></div>
                                <div class="cart_order_step">
                                    <i class="ornament"></i>
                                    <a href="order-step-one.html"><strong>1</strong> Шаг</a>
                                </div>
                                <div class="cart_order_step">
                                    <i class="ornament"></i>
                                    <a href="order-step-two.html"><strong>2</strong> Шаг</a>
                                </div>
                                <div class="cart_order_step active">
                                    <i class="ornament"></i>
                                    <span><strong>3</strong> Шаг</span>
                                </div>
                                <div class="clear"></div>
                                <p><em>Заверщающий шаг!</em></p>
                            </div>

                            <div class="cart_order_form">

                                <fieldset class="reg_address_fieldset">
                                    <p>Контактный<br> адрес:*</p>
                                    <div class="reg_form_right">
                                        <label>Регион / Область</label>
                                        <select class="custom-select" name="region" id="region">
                                            <option value="value1">Челябинская область</option>
                                            <option value="value2">Московская область</option>
                                            <option value="value3">Саратовская область</option>
                                        </select>
                                    </div>
                                    <div class="reg_form_right">
                                        <label>Город</label>
                                        <select class="custom-select" name="city" id="city">
                                            <option value="value1">Магнитогорск</option>
                                            <option value="value2" selected="selected">Челябинск</option>
                                            <option value="value3">Саратов</option>
                                            <option value="value4">Санкт-Петербург</option>
                                        </select>
                                    </div>
                                    <div class="reg_form_right">
                                        <label>Адресс</label>
                                        <input type="text" class="fld" />
                                        <p class="example address_example">например: ул. Хрустальная, 23, под. 5, кв. 98</p>
                                    </div>
                                    <div class="reg_form_right">
                                        <label>Индекс</label>
                                        <input type="text" class="fld" />
                                    </div>
                                    <div class="reg_form_right">
                                        <p class="inlay">Пожалуйста, выберите адрес доставки, тот же, что и контактный</p>
                                    </div>
                                    <div class="reg_form_right">
                                        <p><a class="l_add add_address">Добавить новый адрес доставки</a></p>
                                    </div>
                                    <div class="reg_form_right">
                                        <label class="checkbox">
                                            <input type="checkbox" class="" />
                                            <i>Я согласен на обработку моих персональных данных</i>
                                        </label>
                                    </div>
                                </fieldset>

                                <fieldset class="comment_fieldset">
                                    <p>При желании, Вы можете оставить комментарий к заказу:</p>
                                    <textarea class="fld" name="" id="" cols="30" rows="10"></textarea>
                                    <p class="inlay">После покупки товаров с вами свяжутся и обсудят все вопросы в течение 24 часов, Спасибо!</p>
                                </fieldset>

                            </div>

                            <button class="button order_button" type="submit">
                                <i></i>
                                <span></span>
                                <strong>купить товары »</strong>
                            </button>
                        </form>
                    </section>

                </article>
            </section>
        </div>

    </div>

</div>

</div>