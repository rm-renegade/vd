<link rel="stylesheet" href="/css/gallery.css"/>
<div class="gallery_overlay"></div>

<div class="mini_popup">
    <div class="mini_popup_window">
        <a href="#" class="gallery_popup_close"></a>
        <!-- FORMA -->
        <div class="login_form">
            <form action="" method="POST" name="">
                <fieldset class="chbx">
                    <input type="checkbox" name="" id="chckbx-01" />
                    <label for="chckbx-01">Оформить заказ без регистрации</label>
                </fieldset>

                <h5>Если вы зарегистрированы, <br/>введите:</h5>

                <fieldset>
                    <label for="fld-01">E-mail:</label>
                    <input class="fld" type="text" id="fld-01"/>
                </fieldset>
                <fieldset>
                    <label for="fld-02">Пароль:</label>
                    <input class="fld" type="password" id="fld-02"/>
                    <a id="forgot_pass" href="#">Забыли пароль?</a>
                </fieldset>
                <fieldset class="popup_form_btn">
                    <button type="submit" class="color_btn login_btn right">
                        <span>Войти</span>
                    </button>
                </fieldset>


            </form>
        </div>
        <!-- FORMA END -->
    </div>
</div>