<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="about.html">Магазин</a>
                    <ul class="submenu">
                        <li class="curr"><a href="#">Композиции</a></li>
                        <li><a href="#">Глина</a></li>
                        <li><a href="#">Инструменты</a></li>
                        <li><a href="#">Книги</a></li>
                        <li><a href="#">Аксессуары</a></li>
                        <li><a href="#">ФОМ ЭВА (фоамиран)</a></li>
                        <li><a href="#">Заготовки из пенопласта</a></li>
                        <li><a href="#">Фурнитура</a></li>
                        <li><a href="#">Декор</a></li>
                        <li><a href="#">Кашпо, корзины, вазы</a></li>
                        <li><a href="#">Лента декоративная</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Доставка и оплата</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <div class="page">
                <aside class="aside">
                    <div class="sidebar shadow_medium">
                        <section class="sidebar_widget">
                            <header>Магазин</header>
                            <nav class="sidebar_menu">
                                <ul class="sidebar_menu_mark_list">
                                    <li class="submenulink curr">
                                        <span>Работы Оксаны степановой</span>
                                        <ul class="submenu">
                                            <li class="curr">
                                                <span>композиции</span>
                                            </li>
                                            <li>
                                                <a href="#">свадебные аксессуары</a>
                                            </li>
                                            <li>
                                                <a href="#">украшения</a>
                                            </li>
                                            <li>
                                                <a href="#">куклы</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Глина</a>
                                    </li>
                                    <li>
                                        <a href="#">Инструменты</a>
                                    </li>
                                    <li>
                                        <a href="#">Книги</a>
                                    </li>
                                    <li>
                                        <a href="#">Аксессуары</a>
                                    </li>
                                    <li>
                                        <a href="#">ФОМ ЭВА (фоамиран)</a>
                                    </li>
                                    <li>
                                        <a href="#">Заготовки из пенопласта</a>
                                    </li>
                                    <li>
                                        <a href="#">Фурнитура</a>
                                    </li>
                                    <li>
                                        <a href="#">Декор</a>
                                    </li>
                                    <li>
                                        <a href="#">Кашпо, корзины, вазы</a>
                                    </li>
                                    <li>
                                        <a href="#">Лента декоративная</a>
                                    </li>
                                </ul>
                            </nav>
                        </section>
                    </div>

                    <a class="button delivery_button" href="item-delivery.html">
                        <strong>Доставка и оплата</strong>
                        <i>узнать больше »</i>
                        <span></span>
                    </a>

                    <a class="round_link_pink_aside" href="#"></a>
                </aside>

                <section class="section">
                    <article class="page_content">
                        <hgroup>
                            <h1>Доставка и оплата</h1>
                            <a class="back" href="news.html">Вернуться</a>
                        </hgroup>

                        <section>
                            <h2>Условия и стоимость доставки</h2>
                            <p><strong>Уважаемые клиенты!</strong></p>
                            <p>При оформлении заказа вы можете выбрать удобный для вас вариант доставки. Мы можем предложить вам следующие виды доставки:</p>
                        </section>
                        <section class="delivery_list">
                            <ol class="mark_num">
                                <li>
                                    <p>Самовывоз из офиса</p>
                                    <div class="page_img_txt">
                                        <div class="page_left">
                                            <img src="/i/delivery-01.jpg" width="180" height="80" border="0">
                                            <p>0,00 руб.</p>
                                        </div>
                                        <div class="page_right">
                                            <p>Вы можете самостоятельно забрать свой заказ из творческой матсерской «Вдохновение».</p>
                                            <p>Наш адрес: <br>г.Краснодар, ул.Ставропольская 204 студия 308</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Почта России (до 5кг)</p>
                                    <div class="page_img_txt">
                                        <div class="page_left">
                                            <img src="/i/delivery-02.jpg" width="180" height="80" border="0">
                                            <p>300,00 руб.</p>
                                        </div>
                                        <div class="page_right">
                                            <p>ДОСТАВКА в РЕГИОНЫ осуществляется Почтой России.</p>
                                            <p>Фиксированную сумму доставки в любой город заказов любого веса до 5 кг - 300 рублей!</p>
                                            <p>Максимальная сумма заказа с наложенным платежом - 3000 рублей. Заказы с большей стоимостью мы отправляем по предоплате.</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Почта России (более 5кг)</p>
                                    <div class="page_img_txt">
                                        <div class="page_left">
                                            <img src="/i/delivery-02.jpg" width="180" height="80" border="0">
                                            <p>Оплата рассчитывается индивидуально. </p>
                                        </div>
                                        <div class="page_right">
                                            <p>ДОСТАВКА в РЕГИОНЫ осуществляется Почтой России.</p>
                                            <p>Максимальная сумма заказа с наложенным платежом - 3000 рублей. Заказы с большей стоимостью мы отправляем по предоплате.</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="without_mark">
                                    <p><a href="#">Транспортная компания</a></p>
                                    <div class="page_img_txt">
                                        <div class="page_left">
                                            <img src="/i/delivery-03.jpg" width="180" height="80" border="0">
                                            <p>Оплата рассчитывается индивидуально. </p>
                                        </div>
                                        <div class="page_right">
                                            <p>Вы сами должны нам предоставить контакт своей транспортной компании.</p>
                                        </div>
                                    </div>
                                </li>
                            </ol>
                        </section>

                        <section class="delivery_list">
                            <h2>Оплата</h2>
                            <p>Вы можете совершать покупки в интернет-магазине «Вдохновение» как за наличный расчет, так и по банковским картам. Оплата банковскими картами возможна только при оплате через сайт.</p>
                            <ol class="mark_num">
                                <li>
                                    <p>Оплата безналичным расчетом</p>
                                    <div class="page_img_txt">
                                        <div class="page_left">
                                            <img src="/i/delivery-04.jpg" width="180" height="80" border="0">
                                        </div>
                                        <div class="page_right">
                                            <p>Для оплаты товара банковской картой при оформлении заказа укажите способ оплаты «Оплата банковской картой». Оплата осуществляется непосредственно на сайте сразу после оформления заказа. После подтверждения состава заказа, ваших личных данных и адреса доставки откроется страница, где вам будет предложено ввести данные вашей банковской карты (номер карты, ФИО владельца, срок действия карты, CVV/CVC код)*. После ввода данных карты вам останется только проверить их и нажать кнопку «Оплатить».</p>
                                            <p>Оплата происходит через авторизационный сервер процессингового центра банка с использованием банковских карт следующих платежных систем:
                                                <br/>— VISA
                                                <br/>— MasterCard</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <p>Оплата наличными</p>
                                    <div class="page_img_txt">
                                        <div class="page_left">
                                            <img src="/i/delivery-05.jpg" width="180" height="80" border="0">
                                        </div>
                                        <div class="page_right">
                                            <p>Для оплаты наличными при оформлении заказа укажите способ оплаты «Оплата наличными». Все необходимые финансовые документы вам вручат в офисе. Оплата принимается в российских рублях строго в соответствии с ценой, указанной в кассовом и товарном чеках.</p>
                                        </div>
                                    </div>
                                </li>
                            </ol>
                        </section>
                    </article>
                </section>
            </div>

        </div>

    </div>

</div>