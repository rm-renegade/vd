<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
        BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="about.html">Магазин</a>
                    <ul class="submenu">
                        <li class="curr"><a href="#">Композиции</a></li>
                        <li><a href="#">Глина</a></li>
                        <li><a href="#">Инструменты</a></li>
                        <li><a href="#">Книги</a></li>
                        <li><a href="#">Аксессуары</a></li>
                        <li><a href="#">ФОМ ЭВА (фоамиран)</a></li>
                        <li><a href="#">Заготовки из пенопласта</a></li>
                        <li><a href="#">Фурнитура</a></li>
                        <li><a href="#">Декор</a></li>
                        <li><a href="#">Кашпо, корзины, вазы</a></li>
                        <li><a href="#">Лента декоративная</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li class="breadcrumbs_submenulink">
                    <a href="about.html">Работы Оксаны Степановой</a>
                    <ul class="submenu">
                        <li class="curr"><a href="#">Композиции</a></li>
                        <li><a href="#">Свадебные аксессуары</a></li>
                        <li><a href="#">Украшения</a></li>
                        <li><a href="#">Куклы</a></li>
                    </ul>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Композиции</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Магазин</h2>
                <h4>Подарите радость себе и близким!</h4>
            </hgroup>

            <div class="wrapper ovv">
                <aside class="aside">
                    <div class="sidebar shadow_medium">

                        <section class="sidebar_widget">
                            <header>Магазин</header>
                            <nav class="sidebar_menu">
                                <ul class="sidebar_menu_mark_list">
                                    <li class="submenulink curr">
                                        <span>Работы Оксаны степановой</span>
                                        <ul class="submenu">
                                            <li class="curr">
                                                <span>композиции</span>
                                            </li>
                                            <li>
                                                <a href="#">свадебные аксессуары</a>
                                            </li>
                                            <li>
                                                <a href="#">украшения</a>
                                            </li>
                                            <li>
                                                <a href="#">куклы</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">Глина</a>
                                    </li>
                                    <li>
                                        <a href="#">Инструменты</a>
                                    </li>
                                    <li>
                                        <a href="#">Книги</a>
                                    </li>
                                    <li>
                                        <a href="#">Аксессуары</a>
                                    </li>
                                    <li>
                                        <a href="#">ФОМ ЭВА (фоамиран)</a>
                                    </li>
                                    <li>
                                        <a href="#">Заготовки из пенопласта</a>
                                    </li>
                                    <li>
                                        <a href="#">Фурнитура</a>
                                    </li>
                                    <li>
                                        <a href="#">Декор</a>
                                    </li>
                                    <li>
                                        <a href="#">Кашпо, корзины, вазы</a>
                                    </li>
                                    <li>
                                        <a href="#">Лента декоративная</a>
                                    </li>
                                </ul>
                            </nav>
                        </section>
                    </div>

                    <a class="button delivery_button" href="item-delivery.html">
                        <strong>Доставка и оплата</strong>
                        <i>узнать больше »</i>
                        <span></span>
                    </a>

                    <a class="round_link_pink_aside" href="#"></a>

                </aside>
                <section class="section">

                    <div class="filter_block">
                        <label for="sort_by">Сортировать по:</label>
                        <div class="select sort_by">
                            <select class="custom-select" name="sort_by" id="sort_by">
                                <option value="value1" selected="selected">Дате создания</option>
                                <option value="value2">Стоимости</option>
                            </select>
                        </div>
                        <div class="select sort_items">
                            <select class="custom-select" name="sort_items" id="sort_items">
                                <option value="value1">4 штуки</option>
                                <option value="value2" selected="selected">6 штук</option>
                                <option value="value3">10 штук</option>
                                <option value="value4">20 штук</option>
                            </select>
                        </div>
                        <label for="sort_by">на странице</label>
                        <div class="paginator">
                            <div class="paginator_inner">
                                <ul class="paginator_paging">
                                    <li class="paginator_link paginator_prev">
                                        <a href="#"></a>
                                    </li>
                                    <li class="paginator_link paginator_next">
                                        <a href="#"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="columns catalog">
                        <div class="column">
                            <article class="item shadow_medium">
                                <div class="strip"></div><!-- Strip NEW -->
                                <a href="/magazine/item-page.html" class="item_img">
                                    <img src="/i/magaz-01.jpg">
                                    <!-- span class="overlap">Открыть занятие</span -->
                                </a>
                                <a href="/magazine/item-page.html" class="item_name">Букет «Нежный»</a>
                                <p>Состав: <span>розы, орхидеи, гортензии</span></p>
                                <p>Размер: <span>25 х 4</span></p>

                                <div class="action">
                                    <div class="left_half">
                                        <span>в наличии</span>
                                        <p>5500 руб</p>
                                    </div>
                                    <div class="right_half">
                                        <a class="color_btn coupon_buy" href="#"><span>Купить</span></a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="column">
                            <article class="item shadow_medium">
                                <a href="/magazine/item-page.html" class="item_img">
                                    <img src="/i/magaz-02.jpg">
                                    <!-- span class="overlap">Открыть занятие</span -->
                                </a>
                                <a href="/magazine/item-page.html" class="item_name">Букет «Жемчужные розы»</a>
                                <p>Состав: <span>розы, орхидеи, гортензии</span></p>
                                <p>Размер: <span>25 х 4</span></p>

                                <div class="action">
                                    <div class="left_half">
                                        <span>под заказ</span>
                                        <p>2200 руб</p>
                                    </div>
                                    <div class="right_half">
                                        <a class="color_btn coupon_buy" href="#"><span>Заказать</span></a>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div class="column">
                            <article class="item shadow_medium">
                                <a href="/magazine/item-page.html" class="item_img">
                                    <img src="/i/magaz-03.jpg">
                                    <!-- span class="overlap">Открыть занятие</span -->
                                </a>
                                <a href="/magazine/item-page.html" class="item_name">Букет «Лето в лукошке»</a>
                                <p>Состав: <span>розы, орхидеи, гортензии</span></p>
                                <p>Размер: <span>25 х 4</span></p>

                                <div class="action">
                                    <div class="left_half">
                                        <span>в наличии</span>
                                        <p>5500 руб</p>
                                    </div>
                                    <div class="right_half">
                                        <a class="color_btn coupon_buy" href="#"><span>Купить</span></a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="column">
                            <article class="item shadow_medium">
                                <div class="strip"></div><!-- Strip NEW -->
                                <a href="/magazine/item-page.html" class="item_img">
                                    <img src="/i/magaz-04.jpg">
                                    <!-- span class="overlap">Открыть занятие</span -->
                                </a>
                                <a href="/magazine/item-page.html" class="item_name">Букет «Жемчужные розы»</a>
                                <p>Состав: <span>розы, орхидеи, гортензии</span></p>
                                <p>Размер: <span>25 х 4</span></p>

                                <div class="action">
                                    <div class="left_half">
                                        <span>под заказ</span>
                                        <p>2200 руб</p>
                                    </div>
                                    <div class="right_half">
                                        <a class="color_btn coupon_buy" href="#"><span>Заказать</span></a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="column">
                            <article class="item shadow_medium">
                                <a href="/magazine/item-page.html" class="item_img">
                                    <img src="/i/magaz-05.jpg">
                                    <!-- span class="overlap">Открыть занятие</span -->
                                </a>
                                <a href="/magazine/item-page.html" class="item_name">Букет «Нежный»</a>
                                <p>Состав: <span>розы, орхидеи, гортензии</span></p>
                                <p>Размер: <span>25 х 4</span></p>

                                <div class="action">
                                    <div class="left_half">
                                        <span>в наличии</span>
                                        <p>5500 руб</p>
                                    </div>
                                    <div class="right_half">
                                        <a class="color_btn coupon_buy" href="#"><span>Купить</span></a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="column">
                            <article class="item shadow_medium">
                                <a href="/magazine/item-page.html" class="item_img">
                                    <img src="/i/magaz-06.jpg">
                                    <!-- span class="overlap">Открыть занятие</span -->
                                </a>
                                <a href="/magazine/item-page.html" class="item_name">Букет «Лето в лукошке»</a>
                                <p>Состав: <span>розы, орхидеи, гортензии</span></p>
                                <p>Размер: <span>25 х 4</span></p>

                                <div class="action">
                                    <div class="left_half">
                                        <span>в наличии</span>
                                        <p>5500 руб</p>
                                    </div>
                                    <div class="right_half">
                                        <a class="color_btn coupon_buy" href="#"><span>Купить</span></a>
                                    </div>
                                </div>
                            </article>
                        </div>

                    </div>

                    <?php
                        BLOCK('paginator');
                    ?>

                </section>
            </div>
        </div>
    </div>
</div>