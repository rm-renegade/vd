<?php
/* Содержание сайта */
?>
<div id="conteiner">
<?php
BLOCK('menu');
?>

<div class="white_width">

<div class="breadcrumbs">
    <p>Вы здесь:</p>
    <ul>
        <li>
            <a href="/">Главная</a>
        </li>
        <li class="breadcrumbs_separator">|</li>
        <li class="breadcrumbs_submenulink">
            <a href="/magazine/catalog.html">Магазин</a>
            <ul class="submenu">
                <li class="curr"><a href="#">Композиции</a></li>
                <li><a href="#">Глина</a></li>
                <li><a href="#">Инструменты</a></li>
                <li><a href="#">Книги</a></li>
                <li><a href="#">Аксессуары</a></li>
                <li><a href="#">ФОМ ЭВА (фоамиран)</a></li>
                <li><a href="#">Заготовки из пенопласта</a></li>
                <li><a href="#">Фурнитура</a></li>
                <li><a href="#">Декор</a></li>
                <li><a href="#">Кашпо, корзины, вазы</a></li>
                <li><a href="#">Лента декоративная</a></li>
            </ul>
        </li>
        <li class="breadcrumbs_separator">|</li>
        <li>
            <a href="/magazine/cart.html">Моя корзина</a>
        </li>
        <li class="breadcrumbs_separator">|</li>
        <li>
            <span>Форма заказа</span>
        </li>
    </ul>

    <div class="clear"></div>
</div>

<div class="clear"></div>

<div class="reduser">
    <div class="page">
        <aside class="aside">
            <div class="sidebar shadow_medium">
                <section class="sidebar_widget">
                    <header>Магазин</header>
                    <nav class="sidebar_menu">
                        <ul class="sidebar_menu_mark_list">
                            <li class="submenulink curr">
                                <span>Работы Оксаны степановой</span>
                                <ul class="submenu">
                                    <li class="curr">
                                        <span>композиции</span>
                                    </li>
                                    <li>
                                        <a href="#">свадебные аксессуары</a>
                                    </li>
                                    <li>
                                        <a href="#">украшения</a>
                                    </li>
                                    <li>
                                        <a href="#">куклы</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Глина</a>
                            </li>
                            <li>
                                <a href="#">Инструменты</a>
                            </li>
                            <li>
                                <a href="#">Книги</a>
                            </li>
                            <li>
                                <a href="#">Аксессуары</a>
                            </li>
                            <li>
                                <a href="#">ФОМ ЭВА (фоамиран)</a>
                            </li>
                            <li>
                                <a href="#">Заготовки из пенопласта</a>
                            </li>
                            <li>
                                <a href="#">Фурнитура</a>
                            </li>
                            <li>
                                <a href="#">Декор</a>
                            </li>
                            <li>
                                <a href="#">Кашпо, корзины, вазы</a>
                            </li>
                            <li>
                                <a href="#">Лента декоративная</a>
                            </li>
                        </ul>
                    </nav>
                </section>
            </div>

            <a class="button delivery_button" href="item-delivery.html">
                <strong>Доставка и оплата</strong>
                <i>узнать больше »</i>
                <span></span>
            </a>

            <a class="round_link_pink_aside" href="#"></a>
        </aside>

        <section class="section">
            <article class="page_content">
                <hgroup>
                    <h1>Форма заказа</h1>
                    <h6 class="subtitle">В корзине <span class="cartTotalNum">3 товара</span> <br/>на сумму <span class="cartTotalSum">4 800</span> руб.</h6>
                    <a class="back" href="/magazine/cart.html">Вернуться</a>
                </hgroup>

                <section class="cart_table">
                    <form id="cartOrderForm" class="order_form">

                        <div class="cart_order_steps">
                            <div class="cart_order_steps_strip"></div>
                            <div class="cart_order_step active">
                                <i class="ornament"></i>
                                <span><strong>1</strong> Шаг</span>
                            </div>
                            <div class="cart_order_step">
                                <i class="ornament"></i>
                                <a href="order-step-two.html"><strong>2</strong> Шаг</a>
                            </div>
                            <div class="cart_order_step">
                                <i class="ornament"></i>
                                <a href="order-step-three.html"><strong>3</strong> Шаг</a>
                            </div>
                            <div class="clear"></div>
                            <p><em>Все поля обязательны для заполнения</em></p>
                        </div>

                        <div class="cart_order_form">

                            <fieldset>
                                <p class="radio_title">Способ оплаты:</p>
                                <div class="order_form_right">
                                    <label class="radio_img" for="pay-01">
                                        <input type="radio" id="pay-01" name="payment" value="val1" checked />
                                        <span>Оплата безналичным расчетом</span>
                                        <img src="/i/pay-01.gif" alt=""/>
                                    </label>
                                    <label class="radio_img" for="pay-02">
                                        <input type="radio" id="pay-02" name="payment" value="val2"/>
                                        <span>Оплата наличными</span>
                                        <img src="/i/pay-02.gif" alt=""/>
                                    </label>
                                    <div class="clear"></div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <label>Фамилия*</label>
                                <div class="order_form_right"><input type="text" class="fld fld_full_width" /></div>
                            </fieldset>

                            <fieldset>
                                <label>Имя*</label>
                                <div class="order_form_right"><input type="text" class="fld fld_full_width" /></div>
                            </fieldset>

                            <fieldset>
                                <label>Телефон*</label>
                                <div class="order_form_right">
                                    <span>+7</span>
                                    <input type="text" class="fld fld_code_width" />
                                    <input type="text" class="fld fld_phone_width" />
                                </div>
                                <p class="example phone_example">например: 902 3564021</p>
                            </fieldset>

                            <fieldset>
                                <label>E-mail*</label>
                                <div class="order_form_right"><input type="text" class="fld fld_full_width" /></div>
                            </fieldset>

                        </div>

                        <button class="button order_button" type="submit">
                            <i class="step2"></i>
                            <span></span>
                            <strong>Следующий шаг »</strong>
                        </button>
                    </form>
                </section>

            </article>
        </section>
    </div>

</div>

</div>

</div>