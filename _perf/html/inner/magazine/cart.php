<?php
/* Содержание сайта */
?>
<div id="conteiner">
<?php
BLOCK('menu');
?>

<div class="white_width">

<div class="breadcrumbs">
    <p>Вы здесь:</p>
    <ul>
        <li>
            <a href="/">Главная</a>
        </li>
        <li class="breadcrumbs_separator">|</li>
        <li class="breadcrumbs_submenulink">
            <a href="about.html">Магазин</a>
            <ul class="submenu">
                <li class="curr"><a href="#">Композиции</a></li>
                <li><a href="#">Глина</a></li>
                <li><a href="#">Инструменты</a></li>
                <li><a href="#">Книги</a></li>
                <li><a href="#">Аксессуары</a></li>
                <li><a href="#">ФОМ ЭВА (фоамиран)</a></li>
                <li><a href="#">Заготовки из пенопласта</a></li>
                <li><a href="#">Фурнитура</a></li>
                <li><a href="#">Декор</a></li>
                <li><a href="#">Кашпо, корзины, вазы</a></li>
                <li><a href="#">Лента декоративная</a></li>
            </ul>
        </li>
        <li class="breadcrumbs_separator">|</li>
        <li>
            <span>Моя корзина</span>
        </li>
    </ul>

    <div class="clear"></div>
</div>

<div class="clear"></div>

<div class="reduser">
    <div class="page">
        <aside class="aside">
            <div class="sidebar shadow_medium">
                <section class="sidebar_widget">
                    <header>Магазин</header>
                    <nav class="sidebar_menu">
                        <ul class="sidebar_menu_mark_list">
                            <li class="submenulink curr">
                                <span>Работы Оксаны степановой</span>
                                <ul class="submenu">
                                    <li class="curr">
                                        <span>композиции</span>
                                    </li>
                                    <li>
                                        <a href="#">свадебные аксессуары</a>
                                    </li>
                                    <li>
                                        <a href="#">украшения</a>
                                    </li>
                                    <li>
                                        <a href="#">куклы</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Глина</a>
                            </li>
                            <li>
                                <a href="#">Инструменты</a>
                            </li>
                            <li>
                                <a href="#">Книги</a>
                            </li>
                            <li>
                                <a href="#">Аксессуары</a>
                            </li>
                            <li>
                                <a href="#">ФОМ ЭВА (фоамиран)</a>
                            </li>
                            <li>
                                <a href="#">Заготовки из пенопласта</a>
                            </li>
                            <li>
                                <a href="#">Фурнитура</a>
                            </li>
                            <li>
                                <a href="#">Декор</a>
                            </li>
                            <li>
                                <a href="#">Кашпо, корзины, вазы</a>
                            </li>
                            <li>
                                <a href="#">Лента декоративная</a>
                            </li>
                        </ul>
                    </nav>
                </section>
            </div>

            <a class="button delivery_button" href="item-delivery.html">
                <strong>Доставка и оплата</strong>
                <i>узнать больше »</i>
                <span></span>
            </a>

            <a class="round_link_pink_aside" href="#"></a>
        </aside>

        <section class="section">
            <article class="page_content">
                <hgroup>
                    <h1>Моя корзина</h1>
                    <h6 class="subtitle">В корзине <span class="cartTotalNum"></span> <br/>на сумму <span class="cartTotalSum"></span> руб.</h6>
                    <a class="back" href="">Вернуться</a>
                </hgroup>

                <script type="text/javascript" src="/js/cart.js"></script>
                <section class="cart_table">
                    <form id="cartOrder">
                        <table id="cartOrderTable">
                            <tr>
                                <th class="item_title_row" colspan="2"><span>Товар</span></th>
                                <th class="item_num_row">Кол-во</th>
                                <th class="item_price_row">Цена за 1 шт.</th>
                                <th class="item_sum_row">Общая цена</th>
                            </tr>
                            <tr class="cartItem">
                                <td class="item_img_row">
                                    <div class="cart_item_img">
                                        <span class="cart_item_delete"></span>
                                        <a href="#">
                                            <img src="/i/magaz-14.jpg" width="125" height="80" border="0" alt="" />
                                        </a>
                                    </div>
                                </td>
                                <td class="item_name_row">
                                    <div class="cart_item_descr">
                                        <p><a href="#">Букет «Нежный»</a></p>
                                        <p>Состав: розы, орхидеи, гортензии</p>
                                    </div>
                                </td>
                                <td class="item_num_row">
                                    <div class="input_outer"><input type="text" class="cartItemNum" maxlength="2" value="2" /></div>
                                </td>
                                <td class="item_price_row">
                                    <p><span class="cartItemPrice">4 800</span> руб.</p>
                                </td>
                                <td class="item_sum_row">
                                    <p><span class="cartItemSum"></span> руб.</p>
                                </td>
                            </tr>
                            <tr class="cartItem">
                                <td class="item_img_row">
                                    <div class="cart_item_img">
                                        <span class="cart_item_delete"></span>
                                        <a href="#">
                                            <img src="/i/magaz-15.jpg" width="125" height="80" border="0" alt="" />
                                        </a>
                                    </div>
                                </td>
                                <td class="item_name_row">
                                    <div class="cart_item_descr">
                                        <p><a href="#">Букет «Жемчужные розы»</a></p>
                                        <p>Состав: розы, орхидеи, гортензии</p>
                                    </div>
                                </td>
                                <td class="item_num_row">
                                    <div class="input_outer"><input type="text" class="cartItemNum" maxlength="2" value="2" /></div>
                                </td>
                                <td class="item_price_row">
                                    <p><span class="cartItemPrice">3 200</span> руб.</p>
                                </td>
                                <td class="item_sum_row">
                                    <p><span class="cartItemSum"></span> руб.</p>
                                </td>
                            </tr>
                            <tr class="cartItem">
                                <td class="item_img_row">
                                    <div class="cart_item_img">
                                        <span class="cart_item_delete"></span>
                                        <a href="#">
                                            <img src="/i/magaz-16.jpg" width="125" height="80" border="0" alt="" />
                                        </a>
                                    </div>
                                </td>
                                <td class="item_name_row">
                                    <div class="cart_item_descr">
                                        <p><a href="#">Букет «Нежный»</a></p>
                                        <p>Состав: розы, орхидеи, гортензии</p>
                                    </div>
                                </td>
                                <td class="item_num_row">
                                    <div class="input_outer"><input type="text" class="cartItemNum" maxlength="2" value="2" /></div>
                                </td>
                                <td class="item_price_row">
                                    <p><span class="cartItemPrice">8 900</span> руб.</p>
                                </td>
                                <td class="item_sum_row">
                                    <p><span class="cartItemSum"></span> руб.</p>
                                </td>
                            </tr>
                        </table>
                        <p class="total">Итого: <span class="cartTotalSum"></span> <span>руб.</span></p>

                        <button class="button order_button" type="submit">
                            <i></i>
                            <span></span>
                            <strong>Оформить заказ »</strong>
                        </button>
                    </form>


                </section>

            </article>
        </section>
    </div>

</div>

</div>

</div>