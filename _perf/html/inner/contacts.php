<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
       BLOCK('menu');
    ?>

    <div class="white_width">

        <div class="breadcrumbs">
            <p>Вы здесь:</p>
            <ul>
                <li>
                    <a href="/">Главная</a>
                </li>
                <li class="breadcrumbs_separator">|</li>
                <li>
                    <span>Контакты</span>
                </li>
            </ul>

            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="reduser">
            <hgroup>
                <h2 class="title_ornament">Контакты</h2>
                <h4>Мы всегда рады общению с вами!</h4>
            </hgroup>
            <div class="contacts">
                <div class="contacts_list">
                    <a href="mailto:vdohnovenie-23@mail.ru" class="contact c_mail">
                        <div class="contact_icon">
                            <div class="contact_icon_grey"></div>
                            <div class="contact_icon_blue"></div>
                        </div>
                        <div class="contact_title">e-mail</div>
                        <div class="contact_link">vdohnovenie-23@mail.ru</div>
                    </a>
                    <a href="#" class="contact c_ok">
                        <div class="contact_icon">
                            <div class="contact_icon_grey"></div>
                            <div class="contact_icon_blue"></div>
                        </div>
                        <div class="contact_title">Одноклассники</div>
                        <div class="contact_link">Перейти</div>
                    </a>
                    <a href="#" class="contact c_vk">
                        <div class="contact_icon">
                            <div class="contact_icon_grey"></div>
                            <div class="contact_icon_blue"></div>
                        </div>
                        <div class="contact_title">Вконтакте</div>
                        <div class="contact_link">Перейти</div>
                    </a>
                    <a href="#" class="contact c_fb">
                        <div class="contact_icon">
                            <div class="contact_icon_grey"></div>
                            <div class="contact_icon_blue"></div>
                        </div>
                        <div class="contact_title">facebook</div>
                        <div class="contact_link">Перейти</div>
                    </a>
                    <a href="#" class="contact c_phone">
                        <div class="contact_icon">
                            <div class="contact_icon_grey"></div>
                            <div class="contact_icon_blue"></div>
                        </div>
                        <div class="contact_title">телефон</div>
                        <div class="contact_link">+7 988 1407002</div>
                    </a>
                </div>

                <div class="wrapper">

                    <aside class="aside">

                        <div class="sidebar shadow_medium">
                            <form class="feedback_form sidebar_form" method="POST" action="">
                                <hgroup>
                                    <h3 class="title_strip">Свяжитесь с нами</h3>
                                    <p>Если вы хотите задать вопрос или записаться на мастер-класс, заполните форму ниже и мы свяжемся с вами</p>
                                </hgroup>

                                <fieldset>
                                    <input type="text" class="fld" value="Введите ваше имя" />
                                </fieldset>

                                <fieldset>
                                    <input type="text" class="fld" value="Введите ваше e-mail" />
                                </fieldset>

                                <fieldset>
                                    <textarea class="fld">Напишите ваш отзыв</textarea>
                                </fieldset>

                                <fieldset>
                                    <p>Введите код с картинки:</p>
                                    <div>
                                        <img class="captcha" src="/i/captcha.gif">
                                        <input type="text" class="fld fld_captcha_width" />
                                    </div>
                                </fieldset>

                                <button type="submit" class="button sidebar_button">
                                    <i></i>
                                    <span></span>
                                    <strong>Отправить</strong>
                                </button>
                            </form>
                        </div>

                    </aside>

                    <section class="section">
                        <hgroup>
                            <h3 class="title_strip">Приходите к нам в студию</h3>
                            <p>Мы всегда рады новым людям, приходите к нам и насладитесь творческой обстановкой нашей мастерской «Вдохновение», познакомьтесь с новыми людьми и зарядитесь хорошим настроением. Мы ждем вас!</p>
                            <address>Наш адрес - Краснодарский край, г.Краснодар, ул.Ставропольская 204 студия 308</address>
                        </hgroup>

                        <div class="map">
                            <script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=dqve8nja6n88lJ1PxdKyu_1qN5rpYYcs&width=100%&height=600"></script>
                        </div>

                    </section>

                </div>
            </div>

        </div>
    </div>
</div>