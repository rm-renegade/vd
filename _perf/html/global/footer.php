<div id="footer_spacer"></div>
<footer id="footer">
    <div class="reduser">
        <a href="/" class="f_logo"></a>

        <ul class="f_social">
            <li class="f_social_item"><a class="f_mail" href="#"></a></li>
            <li class="f_social_item"><a class="f_ok" href="#"></a></li>
            <li class="f_social_item"><a class="f_vk" href="#"></a></li>
            <li class="f_social_item"><a class="f_fb" href="#"></a></li>
        </ul>

        <div class="f_phone"><i class="f_phone_icon"></i> +7 988 1407002</div>
        <address>
            <p>Краснодарский край, г.Краснодар,<br> ул.Ставропольская 204 студия 308</p>
        </address>
        <p>e-mail: <a href="mailto:vdohnovenie-23@mail.ru">vdohnovenie-23@mail.ru</a></p>
        <br/>
        <br/>
        <p>© 2013 Вдохновение </p>

    </div>
</footer>