<?php
/* Содержание сайта */
?>
<div id="conteiner">
    <?php
       BLOCK('menu');
    ?>

    <div class="brick_width">
        <div class="reduser">
            <div class="promo">
                <h2 class="title_ornament">Открой&nbsp;для&nbsp;себя<br/>новые&nbsp;возможности&nbsp;прямо&nbsp;сейчас!</h2>
                <div class="columns">
                    <div class="column">
                        <a class="promo_item" href="/education/education.html">
                            <div class="promo_picture">
                                <img src="/i/promo-01-dark.jpg" width="325" height="145"><!-- Обычная картинка -->
                                <img class="promo_hover" src="/i/promo-01.jpg" width="325" height="145"><!-- Ховер картинка -->
                            </div>
                            <div class="promo_icon education"></div>
                            <span>Обучение</span>
                        </a>
                    </div>
                    <div class="column">
                        <a class="promo_item" href="/gallery/gallery.html">
                            <div class="promo_picture">
                                <img src="/i/promo-02-dark.jpg" width="325" height="145"><!-- Обычная картинка -->
                                <img class="promo_hover" src="/i/promo-02.jpg" width="325" height="145"><!-- Ховер картинка -->
                            </div>
                            <div class="promo_icon gallery"></div>
                            <span>Галерея работ</span>
                        </a>
                    </div>
                    <div class="column">
                        <a class="promo_item" href="/magazine/catalog.html">
                            <div class="promo_picture">
                                <img src="/i/promo-03-dark.jpg" width="325" height="145"><!-- Обычная картинка -->
                                <img class="promo_hover" src="/i/promo-03.jpg" width="325" height="145"><!-- Ховер картинка -->
                            </div>
                            <div class="promo_icon magazine"></div>
                            <span>Магазин</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--div class="white_width"><!-- Этот класс используем, если нужно на всю ширину тянуть -->
        <?php
            BLOCK('sertificate');
        ?>
    <!--/div-->

    <div class="brick_width">
        <div class="reduser">
            <div class="news news_last">
                <h2 class="title_ornament">Узнай о нас все самое интересное <br> в наших новостях!</h2>
                <div class="columns flexslider" id="last_news">
                    <ul class="slides">
                        <li>
                            <div class="column">
                                <article class="article article-main">
                                    <a href="/workshop/news-page.html" class="article_link">
                                        <div class="article_img">
                                            <img src="/i/article-01.jpg">
                                            <div class="overlap">Открыть статью</div>
                                        </div>
                                        <p>Почему параллелен социальная психология искусства?</p>
                                    </a>
                                    <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
                                </article>
                            </div>
                            <div class="column">
                                <article class="article article-main">
                                    <a href="/workshop/news-page.html" class="article_link">
                                        <div class="article_img">
                                            <img src="/i/article-02.jpg">
                                            <div class="overlap">Открыть статью</div>
                                        </div>
                                        <p>Деструктивный экспрессионизм: эмпирическая история искусств?</p>
                                    </a>
                                    <em><time pubdate datetime="2013-07-06">2 января, 2013г.</time></em>
                                </article>
                            </div>
                            <div class="column">
                                <article class="article article-main">
                                    <a href="/workshop/news-page.html" class="article_link">
                                        <div class="article_img">
                                            <img src="/i/article-03.jpg">
                                            <div class="overlap">Открыть статью</div>
                                        </div>
                                        <p>Фактографический экзистенциализм: методология и особенности</p>
                                    </a>
                                    <em><time pubdate datetime="2013-07-06">26 сентября, 2012г.</time></em>
                                </article>
                            </div>
                        </li>
                        <li>
                            <div class="column">
                                <article class="article article-main">
                                    <a href="/workshop/news-page.html" class="article_link">
                                        <div class="article_img">
                                            <img src="/i/article-03.jpg">
                                            <div class="overlap">Открыть статью</div>
                                        </div>
                                        <p>Фактографический экзистенциализм: методология и особенности</p>
                                    </a>
                                    <em><time pubdate datetime="2013-07-06">26 сентября, 2012г.</time></em>
                                </article>
                            </div>
                            <div class="column">
                                <article class="article article-main">
                                    <a href="/workshop/news-page.html" class="article_link">
                                        <div class="article_img">
                                            <img src="/i/article-01.jpg">
                                            <div class="overlap">Открыть статью</div>
                                        </div>
                                        <p>Почему параллелен социальная психология искусства?</p>
                                    </a>
                                    <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
                                </article>
                            </div>
                            <div class="column">
                                <article class="article article-main">
                                    <a href="/workshop/news-page.html" class="article_link">
                                        <div class="article_img">
                                            <img src="/i/article-02.jpg">
                                            <div class="overlap">Открыть статью</div>
                                        </div>
                                        <p>Деструктивный экспрессионизм: эмпирическая история искусств?</p>
                                    </a>
                                    <em><time pubdate datetime="2013-07-06">2 января, 2013г.</time></em>
                                </article>
                            </div>
                        </li>
                        <li>
                            <div class="column">
                                <article class="article article-main">
                                    <a href="/workshop/news-page.html" class="article_link">
                                        <div class="article_img">
                                            <img src="/i/article-02.jpg">
                                            <div class="overlap">Открыть статью</div>
                                        </div>
                                        <p>Деструктивный экспрессионизм: эмпирическая история искусств?</p>
                                    </a>
                                    <em><time pubdate datetime="2013-07-06">2 января, 2013г.</time></em>
                                </article>
                            </div>
                            <div class="column">
                                <article class="article article-main">
                                    <a href="/workshop/news-page.html" class="article_link">
                                        <div class="article_img">
                                            <img src="/i/article-01.jpg">
                                            <div class="overlap">Открыть статью</div>
                                        </div>
                                        <p>Почему параллелен социальная психология искусства?</p>
                                    </a>
                                    <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
                                </article>
                            </div>
                            <div class="column">
                                <article class="article article-main">
                                    <a href="/workshop/news-page.html" class="article_link">
                                        <div class="article_img">
                                            <img src="/i/article-03.jpg">
                                            <div class="overlap">Открыть статью</div>
                                        </div>
                                        <p>Фактографический экзистенциализм: методология и особенности</p>
                                    </a>
                                    <em><time pubdate datetime="2013-07-06">26 сентября, 2012г.</time></em>
                                </article>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="clear"></div>
                <div class="more"><a href="/workshop/news.html">Посмотреть все новости</a></div>
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('#last_news').flexslider({
                            animation: "fade",
                            easing: "swing",
                            directionNav: false
                        });
                    });
                </script>
            </div>
        </div>
    </div>

</div>