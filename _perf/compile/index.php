<title>����� �������...</title>
<?php
$ROOT_DIR = (substr($_SERVER['DOCUMENT_ROOT'], (strlen($_SERVER['DOCUMENT_ROOT']) - 1)) == "/") ? $_SERVER['DOCUMENT_ROOT'] : $_SERVER['DOCUMENT_ROOT'].'/';
include $ROOT_DIR.'_perf/config.php';
include $ROOT_DIR.'_perf/bin/fns.php';

deleteDirsAndFiles($ROOT_DIR, $ROOT_DIR, $SYSTEM_FOLDER);

$len = count($FILE_LIST);
for($i=0; $i<$len; $i++) {
	$folder = ( isset($FILE_LIST[$i]['folder']) && trim($FILE_LIST[$i]['folder']) != '' ) ? trim($FILE_LIST[$i]['folder']).'/' : '';
	if($folder != '') {
		if(!is_dir($ROOT_DIR.$folder)) {
			mkdir($ROOT_DIR.$folder);
		}
	}
	
	/* generation files */
	foreach($FILE_LIST[$i]['files'] as $file_name => $PAGE) {
		$file_path = $ROOT_DIR.$folder.$file_name;
		$short_file_path = $folder.$file_name;
		$INCLUDES = $PAGE['includes'];
		
		ob_start();
		include $ROOT_DIR.'_perf/layout.php';
		$HTML = ob_get_contents();
		ob_end_clean();
		
		$new_html_file = fopen($file_path,'w+');
		fputs($new_html_file,$HTML);
		fclose($new_html_file);
		unset($HTML);
	}
}

/* build css */

/*
$SYSTEM_FILES = array(
	'css'=>array(
		'folder'=>'css',
		'compile'=>'main.css'
	)
);
*/
foreach($SYSTEM_FILES as $folder => $data) {
	$fld = $ROOT_DIR.$data['folder'];
	if( !is_dir( $fld ) ) {
		mkdir( $fld );
	}
	
	
	$directory = $ROOT_DIR.'_perf/'.$folder;
	$dir = opendir($directory);
	while(($file = readdir($dir))){
		if ( is_file ($directory.'/'.$file)){
			copy ( $directory.'/'.$file, $ROOT_DIR.$data['folder'].'/'.$file );
		}
	}
	closedir ($dir);
	
	$str = "\r\n";
	$directory = $ROOT_DIR.'_perf/temp';
	$dir = opendir($directory);
	while(($file = readdir($dir))){
		if ( is_file ($directory.'/'.$file)){
			$str .= file_get_contents($directory.'/'.$file)."\r\n";
		}
	}
	closedir ($dir);
	
	$new_html_file = fopen($fld.'/'.$data['compile'],'a');
	fputs($new_html_file,$str);
	fclose($new_html_file);
	
	$directory = $ROOT_DIR.'_perf/temp';
	$dir = opendir($directory);
	while(($file = readdir($dir))){
		if ( is_file ($directory.'/'.$file)){
			@unlink($directory.'/'.$file);
		}
	}
	closedir ($dir);
}

?>