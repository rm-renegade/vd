<?php
/* Удаление всех скомпилированных файлов и папок */
function deleteDirsAndFiles($directory, $root, $folders) {
	$dir = opendir($directory);
	while(($file = readdir($dir))){
		if ( is_file ($directory.'/'.$file) && !in_array ( $file , $folders ) ){
			unlink($directory.'/'.$file);
		}
		else if(is_dir ($directory.'/'.$file) && ($file != '.') && ($file != '..') && !in_array ( $file , $folders )){
			deleteDirsAndFiles($directory.'/'.$file, $root, $folders);  
		}
	}
	closedir ($dir);
	if($directory != $root) {
		rmdir ($directory);
	}
}

/* Вставляет на страницу файл с логическим блоком */
function INSERT($path, $short_file_path,$fld) {
	$ROOT_DIR = (substr($_SERVER['DOCUMENT_ROOT'], (strlen($_SERVER['DOCUMENT_ROOT']) - 1)) == "/") ? $_SERVER['DOCUMENT_ROOT'] : $_SERVER['DOCUMENT_ROOT'].'/';
	$folder = $ROOT_DIR.'_perf/html/';
	
	if( trim($path) != '' && is_file($folder.$path) ) {
		include $folder.$path;
	}
}

/*
function LINK($curr_page, $curr_folder, $page, $link_code, $curr_page_code) {
	$ROOT_DIR = (substr($_SERVER['DOCUMENT_ROOT'], (strlen($_SERVER['DOCUMENT_ROOT']) - 1)) == "/") ? $_SERVER['DOCUMENT_ROOT'] : $_SERVER['DOCUMENT_ROOT'].'/';
	
	//print $curr_page.' '.$curr_folder.' ';
	
	if($curr_page==$page ||$curr_folder==$page) {
		if(is_file( $ROOT_DIR.'_perf/'.$curr_page_code )) {
			include $ROOT_DIR.'_perf/'.$curr_page_code;
		}
		else {
			return $curr_page_code;
		}
	}
	else {
		if(is_file( $ROOT_DIR.'_perf/'.$link_code )) {
			include $ROOT_DIR.'_perf/'.$link_code;
		}
		else {
			return $link_code;
		}
	}
}
*/

function BLOCK($path,$BLOCK=array()) {
	$ROOT_DIR = (substr($_SERVER['DOCUMENT_ROOT'], (strlen($_SERVER['DOCUMENT_ROOT']) - 1)) == "/") ? $_SERVER['DOCUMENT_ROOT'] : $_SERVER['DOCUMENT_ROOT'].'/';
	
	$block = $ROOT_DIR.'_perf/block/'.$path.'/'.$path.'.php';
	$css = $ROOT_DIR.'_perf/block/'.$path.'/'.$path.'.css';
	$temp_css = $ROOT_DIR.'_perf/temp/'.$path.'.css';

	include $block;
	
	if( is_file( $css ) ) {
		if( !is_file( $temp_css ) ) {
			copy ( $css, $temp_css );
		}
	}
}

function HREF($page_id) {
	$ROOT_DIR = (substr($_SERVER['DOCUMENT_ROOT'], (strlen($_SERVER['DOCUMENT_ROOT']) - 1)) == "/") ? $_SERVER['DOCUMENT_ROOT'] : $_SERVER['DOCUMENT_ROOT'].'/';
	include $ROOT_DIR.'_perf/config.php';
	
	$len = count($FILE_LIST);
	for($i=0; $i<$len; $i++) {
		$folder = ( isset($FILE_LIST[$i]['folder']) && trim($FILE_LIST[$i]['folder']) != '' ) ? '/'.trim($FILE_LIST[$i]['folder']).'/' : '/';
		
		foreach($FILE_LIST[$i]['files'] as $file_name => $PAGE) {
			if( (int)$page_id == (int)$PAGE['id'] ) {
				print $folder.$file_name;
				break;
			}
		}
	}
}
?>