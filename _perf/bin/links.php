<title>Список ссылок</title>
<?php
$ROOT_DIR = (substr($_SERVER['DOCUMENT_ROOT'], (strlen($_SERVER['DOCUMENT_ROOT']) - 1)) == "/") ? $_SERVER['DOCUMENT_ROOT'] : $_SERVER['DOCUMENT_ROOT'].'/';
include $ROOT_DIR.'_perf/config.php';

$server = $_SERVER['HTTP_HOST'];

$counter = 0;
$len = count($FILE_LIST);
for($i=0; $i<$len; $i++) {
	$folder = $FILE_LIST[$i];
	$f_len = count($folder);
	if($f_len>0) {
		if( isset($folder['name']) && trim($folder['name']) != '' ) {
			print '<h2 style="padding: 30px 0px 5px; margin: 0px;">'.trim($folder['name']).'</h2>'."\n\n";
		}
		else {
			print '<div style="padding: 30px 0px 5px; margin: 0px;"></div>'."\n\n";
		}
		$folder_path = ( isset($folder['folder']) && trim($folder['folder']) != '' ) ? '/'.$folder['folder'] : '';
		//print '<ul style="padding: 0px; margin: 0px;">';
		foreach($folder['files'] as $key => $val) {
			$counter++;
			print '<div>'.$counter.' <a href="http://'.$server.$folder_path.'/'.$key.'">http://'.$server.$folder_path.'/'.$key.'</a></div>';
		}
		//print '</ul>';
	}
}
print '<h2 style="padding: 30px 0px 5px; margin: 0px;">Всего: '.$counter.' страниц</h2>'."\n\n";
?>