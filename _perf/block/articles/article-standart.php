<article class="article article-main">
    <a href="/workshop/news-page.html" class="article_link">
        <div class="article_img">
            <?php
                BLOCK('/_perf/block/article-figure/article-figure.php',array(
                    'src'=>$BLOCK['src'],
                    'width'=>$BLOCK['width'],
                    'height'=>$BLOCK['height']
                ));
            ?>
            <div class="overlap">Открыть статью</div>
        </div>
        <p><?= $BLOCK['title']; ?></p>
    </a>
    <em><time pubdate datetime=""><?= $BLOCK['date']; ?></time></em>
</article>