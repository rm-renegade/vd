<?php
/* Paginator */
?>

<div class="paginator">
    <div class="paginator_inner">

        <ul class="paginator_paging">

            <li class="paginator_link paginator_prev">
                <a href="#"></a>
            </li>

            <li class="paginator_link curr">
                <a href="#">1</a>
            </li>
            <li class="paginator_link">
                <a href="#">2</a>
            </li>
            <li class="paginator_link">
                <a href="#">3</a>
            </li>
            <li class="paginator_link">
                <a href="#">4</a>
            </li>

            <li class="paginator_link paginator_next">
                <a href="#"></a>
            </li>

        </ul>

    </div>
</div>