<?php
/* Head static */
?>
<div class="top">
    <div class="search_block">
        <form action="" method="POST" id="search_form">
            <input class="search_fld" type="text" value="Что вы ищете?"/>
            <button class="search_btn" type="submit" value=""></button>
        </form>
    </div>
    <ul class="h_social">
        <li class="h_social_item"><a class="h_mail" href="#"></a></li>
        <li class="h_social_item"><a class="h_ok" href="#"></a></li>
        <li class="h_social_item"><a class="h_vk" href="#"></a></li>
        <li class="h_social_item"><a class="h_fb" href="#"></a></li>
    </ul>
    <div class="h_phone"><i class="h_phone_icon"></i> +7 988 1407002</div>
</div>
<div class="clear"></div>

<div id="login_block">
    <a href="#">Вход</a>
    <span>|</span>
    <a href="/registration.html">Регистрация</a>
</div>
<div class="clear"></div>

<div id="cart" class="full">
    <div class="cart_icon"></div>
    <div class="cart_title">Моя корзина</div>
    <div class="cart_full">
        <p class="cart_num"><span class="cartTotalNum">2 товара</span></p>
        <p class="cart_sum"><span class="cartTotalSum">5180</span> руб</p>
    </div>
    <div class="cart_empty">
        <p>Корзина пуста</p>
    </div>
</div>