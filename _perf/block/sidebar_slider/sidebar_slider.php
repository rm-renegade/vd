<?php
/* Sidebar Slider */
?>
<div class="widget_slider flexslider" id="widget_slider">
    <ul class="slides">
        <li>
            <article class="article">
                <a href="#" class="article_link">
                    <div class="article_img">
                        <img src="/i/article-min-01.jpg">
                        <div class="overlap">Открыть статью</div>
                    </div>
                    <p>Почему параллелен социальная психология искусства?</p>
                </a>
                <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
            </article>
            <article class="article">
                <a href="#" class="article_link">
                    <div class="article_img">
                        <img src="/i/article-min-02.jpg">
                        <div class="overlap">Открыть статью</div>
                    </div>
                    <p>Почему параллелен социальная психология искусства?</p>
                </a>
                <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
            </article>
            <article class="article">
                <a href="#" class="article_link">
                    <div class="article_img">
                        <img src="/i/article-min-03.jpg">
                        <div class="overlap">Открыть статью</div>
                    </div>
                    <p>Почему параллелен социальная психология искусства?</p>
                </a>
                <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
            </article>
        </li>
        <li>
            <article class="article">
                <a href="#" class="article_link">
                    <div class="article_img">
                        <img src="/i/article-min-04.jpg">
                        <div class="overlap">Открыть статью</div>
                    </div>
                    <p>Почему параллелен социальная психология искусства?</p>
                </a>
                <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
            </article>
            <article class="article">
                <a href="#" class="article_link">
                    <div class="article_img">
                        <img src="/i/article-min-05.jpg">
                        <div class="overlap">Открыть статью</div>
                    </div>
                    <p>Почему параллелен социальная психология искусства?</p>
                </a>
                <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
            </article>
            <article class="article">
                <a href="#" class="article_link">
                    <div class="article_img">
                        <img src="/i/article-min-06.jpg">
                        <div class="overlap">Открыть статью</div>
                    </div>
                    <p>Почему параллелен социальная психология искусства?</p>
                </a>
                <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
            </article>
        </li>
        <li>
            <article class="article">
                <a href="#" class="article_link">
                    <div class="article_img">
                        <img src="/i/article-min-06.jpg">
                        <div class="overlap">Открыть статью</div>
                    </div>
                    <p>Почему параллелен социальная психология искусства?</p>
                </a>
                <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
            </article>
            <article class="article">
                <a href="#" class="article_link">
                    <div class="article_img">
                        <img src="/i/article-min-03.jpg">
                        <div class="overlap">Открыть статью</div>
                    </div>
                    <p>Почему параллелен социальная психология искусства?</p>
                </a>
                <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
            </article>
            <article class="article">
                <a href="#" class="article_link">
                    <div class="article_img">
                        <img src="/i/article-min-01.jpg">
                        <div class="overlap">Открыть статью</div>
                    </div>
                    <p>Почему параллелен социальная психология искусства?</p>
                </a>
                <em><time pubdate datetime="2013-07-06">6 июля, 2013г.</time></em>
            </article>
        </li>
    </ul>
</div>

<div class="clear"></div>
<!-- FlexSlider -->
<script defer src="/js/jquery.flexslider-min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#widget_slider').flexslider({
            animation: "fade",
            easing: "swing",
            directionNav: false
        });
    });
</script>