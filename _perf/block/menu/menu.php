<?php
/* Main Menu */
?>
<nav id="menu" class="shadow_long">
    <ul class="menu">
        <li class="curr">
            <span>Главная</span>
        </li>
        <li class="submenulink">
            <a href="/workshop/about.html">О мастерской</a>
            <ul class="submenu">
                <li><a href="/workshop/about.html">О нас</a></li>
                <li><a href="/workshop/news.html">Новости</a></li>
                <li><a href="/workshop/reviews.html">Отзывы</a></li>
                <li><a href="/workshop/pressa.html">Пресса о мастерской</a></li>
                <li><a href="#">Выставки</a></li>
                <li><a href="#">Дипломы и награды</a></li>
            </ul>
        </li>
        <li class="submenulink">
            <a href="/education/education.html">Обучение</a>
            <ul class="submenu">
                <li class="sublevelmenulink">
                    <a href="/education/education.html">Курс обучения</a>
                    <ul class="sublevelmenu">
                        <li><a href="/education/education-page.html">Лепка CLAYCRAFT by DECO</a></li>
                        <li><a href="/education/education-page.html">Лепка FIMO</a></li>
                        <li><a href="/education/education-page.html">Декупаж</a></li>
                        <li><a href="/education/education-page.html">Валяние</a></li>
                        <li><a href="/education/education-page.html">Вышивка лентами</a></li>
                        <li><a href="/education/education-page.html">Мыловарение</a></li>
                        <li><a href="/education/education-page.html">Фоармин</a></li>
                        <li><a href="/education/education-page.html">Психология</a></li>
                    </ul>
                </li>
                <li>
                    <a href="/education/timetable.html">Расписание</a>
                </li>
                <li>
                    <a href="/education/coupon.html">Подарочный купон</a>
                </li>
            </ul>
        </li>
        <li class="submenulink">
            <a href="/magazine/catalog.html">Магазин</a>
            <ul class="submenu">
                <li class="sublevelmenulink">
                    <a href="#">Работы Оксаны Степановой</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Композиции</a></li>
                        <li><a href="#">Свадебные аксессуары</a></li>
                        <li><a href="#">Украшения</a></li>
                        <li><a href="#">Куклы</a></li>
                    </ul>
                </li>
                <li class="sublevelmenulink">
                    <a href="#">Глина</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Композиции</a></li>
                        <li><a href="#">Свадебные аксессуары</a></li>
                        <li><a href="#">Куклы</a></li>
                    </ul>
                </li>
                <li class="sublevelmenulink">
                    <a href="#">Инструменты</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Композиции</a></li>
                        <li><a href="#">Украшения</a></li>
                        <li><a href="#">Куклы</a></li>
                    </ul>
                </li>
                <li class="sublevelmenulink">
                    <a href="#">Книги</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Украшения</a></li>
                        <li><a href="#">Куклы</a></li>
                    </ul>
                </li>
                <li class="sublevelmenulink">
                    <a href="#">Аксессуары</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Композиции</a></li>
                        <li><a href="#">Свадебные аксессуары</a></li>
                    </ul>
                </li>
                <li class="sublevelmenulink">
                    <a href="#">ФОМ ЭВА (фоамиран)</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Свадебные аксессуары</a></li>
                        <li><a href="#">Украшения</a></li>
                        <li><a href="#">Куклы</a></li>
                    </ul>
                </li>
                <li class="sublevelmenulink">
                    <a href="#">Заготовки из пенопласта</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Композиции</a></li>
                        <li><a href="#">Украшения</a></li>
                    </ul>
                </li>
                <li class="sublevelmenulink">
                    <a href="#">Фурнитура</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Свадебные аксессуары</a></li>
                        <li><a href="#">Украшения</a></li>
                        <li><a href="#">Куклы</a></li>
                    </ul>
                </li>
                <li class="sublevelmenulink">
                    <a href="#">Декор</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Свадебные аксессуары</a></li>
                        <li><a href="#">Куклы</a></li>
                    </ul>
                </li>
                <li class="sublevelmenulink">
                    <a href="#">Кашпо, корзины, вазы</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Композиции</a></li>
                        <li><a href="#">Свадебные аксессуары</a></li>
                        <li><a href="#">Украшения</a></li>
                    </ul>
                </li>
                <li class="sub_item">
                    <a href="#">Лента декоративная</a>
                    <ul class="sublevelmenu">
                        <li><a href="#">Композиции</a></li>
                        <li><a href="#">Свадебные аксессуары</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <a href="/gallery/gallery.html">Галерея</a>
        </li>
        <li>
            <a href="/trust/trust.html">Нам доверяют</a>
        </li>
        <li>
            <a href="/blog/blog.html">Блог</a>
        </li>
        <li>
            <a href="/contacts.html">Контакты</a>
        </li>
    </ul>
</nav>
