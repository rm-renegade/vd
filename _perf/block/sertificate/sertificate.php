<?php
/* MainPage Sertificate */
?>
<div class="sertificate shadow_long">
    <div class="reduser">
        <h3><img src="/i/sertificate-title.png" width="776" height="54" alt="купи Подарочный сертификат"></h3>
        <ul>
            <li>
                <span>1</span>
                <p>
                    Поздравьте
                    <strong>близких</strong>
                    <strong>и коллег</strong>
                </p>
            </li>
            <li>
                <span>2</span>
                <p>
                    порадуйте
                    <strong>уникальным</strong>
                    <strong>подарком</strong>
                </p>
            </li>
            <li>
                <span>3</span>
                <p>
                    окунитесь
                    <strong>в мир</strong>
                    <strong>творчества</strong>
                </p>
            </li>
        </ul>
        <a href="#" class="round_link_pink"></a>
    </div>
</div>