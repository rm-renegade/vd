<?php
/* Flex Slider */
?>

<section class="slider">
    <div class="flexslider" id="main_slider">
        <ul class="slides">
            <li class="slide_item" id="">
                <img src="/i/main-slide-01.jpg" class="slide_img">
                <div class="reduser">
                    <div class="slide_title">
                        <img src="/i/main-slide-title-01.png" width="488" height="122" alt="... там, где живое искусство">
                    </div>
                    <a class="slide_link" href="#">подарите радость близким</a>
                </div>
            </li>
            <li class="slide_item" id="">
                <img src="/i/main-slide-02.jpg" class="slide_img">
                <div class="reduser">
                    <div class="slide_title">
                        <img src="/i/main-slide-title-02.png" width="464" height="191" alt="Научись создавать красоту своими руками">
                    </div>
                    <a class="slide_link" href="#">Посмотреть все мастер-классы</a>
                </div>
            </li>
            <li class="slide_item" id="">
                <img src="/i/main-slide-03.jpg" class="slide_img">
                <div class="reduser">
                    <div class="slide_title">
                        <img src="/i/main-slide-title-03.png" width="463" height="133" alt="Сделано руками, согрето душой">
                    </div>
                    <a class="slide_link" href="#">Посмотреть все работы</a>
                </div>
            </li>
            <li class="slide_item" id="">
                <img src="/i/main-slide-04.jpg" class="slide_img">
                <div class="reduser">
                    <div class="slide_title">
                        <img src="/i/main-slide-title-04.png" width="522" height="122" alt="... там, где витает вдохновение">
                    </div>
                    <a class="slide_link" href="#">Узнать больше о мастерской</a>
                </div>
            </li>
        </ul>
    </div>



    <!-- jQuery -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <!-- script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.min.js">\x3C/script>')</script -->

    <!-- FlexSlider -->
    <script defer src="/js/jquery.flexslider-min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#main_slider').flexslider({
                animation: "slide",
                directionNav: false,
                start: function(slider){
                    /* todo: */
                    //console.log(123);
                }
            });
        });
    </script>

</section>