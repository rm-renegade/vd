<?php
/* Список директорий, к которым не имеет доступа compile.php */
$SYSTEM_FOLDER = array(
    'i',
    'pict',
    'js',
    '_perf',
    'psd',
    '.gitignore',
    '.git',
    '.htaccess'
);

$SYSTEM_FILES = array(
    'css'=>array(
        'folder'=>'css',
        'compile'=>'main.css'
    )
);


/* Список файлов, из которых собираются HTML-файлы */
$FILE_LIST = array(
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'files'=> array(
            'index.html'=>array(
                'id'=>1,
                'title'=>'Вдохновение. Творческая мастерская Оксаны Степановой',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'index/header.php',
                    'content'=>'index/content.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'files'=> array(
            'contacts.html'=>array(
                'id'=>2,
                'title'=>'Вдохновение. Контакты',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/contacts.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'files'=> array(
            'registration.html'=>array(
                'id'=>3,
                'title'=>'Вдохновение. Регистрация',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/registration.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'workshop',
        'name'=>'О мастерской',
        'files'=> array(
            'about.html'=>array(
                'id'=>4,
                'title'=>'Вдохновение. О мастерской. О нас',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/workshop/about.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'workshop',
        'name'=>'О мастерской',
        'files'=> array(
            'news.html'=>array(
                'id'=>5,
                'title'=>'Вдохновение. О мастерской. Новости',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/workshop/news.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'workshop',
        'name'=>'О мастерской',
        'files'=> array(
            'news-page.html'=>array(
                'id'=>6,
                'title'=>'Вдохновение. О мастерской. Новости. Страница',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/workshop/news-page.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'workshop',
        'name'=>'О мастерской',
        'files'=> array(
            'reviews.html'=>array(
                'id'=>7,
                'title'=>'Вдохновение. О мастерской. Отзывы',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/workshop/reviews.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'workshop',
        'name'=>'О мастерской',
        'files'=> array(
            'pressa.html'=>array(
                'id'=>8,
                'title'=>'Вдохновение. О мастерской. Пресса о нас',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/workshop/pressa.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'trust',
        'name'=>'Нам доверяют',
        'files'=> array(
            'trust.html'=>array(
                'id'=>9,
                'title'=>'Вдохновение. Нам доверяют',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/trust/trust.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'trust',
        'name'=>'Нам доверяют',
        'files'=> array(
            'trust-page.html'=>array(
                'id'=>10,
                'title'=>'Вдохновение. Нам доверяют. Страница',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/trust/trust-page.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'blog',
        'name'=>'Блог',
        'files'=> array(
            'blog.html'=>array(
                'id'=>11,
                'title'=>'Вдохновение. Блог',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/blog/blog.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'blog',
        'name'=>'Блог',
        'files'=> array(
            'blog-page.html'=>array(
                'id'=>12,
                'title'=>'Вдохновение. Блог. Страница',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/blog/blog-page.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'blog',
        'name'=>'Блог',
        'files'=> array(
            'faq.html'=>array(
                'id'=>13,
                'title'=>'Вдохновение. Часто задаваемые вопросы',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/blog/faq.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'education',
        'name'=>'Обучение',
        'files'=> array(
            'education.html'=>array(
                'id'=>14,
                'title'=>'Вдохновение. Обучение. Курс обучения',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/education/education.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'education',
        'name'=>'Обучение',
        'files'=> array(
            'education-page.html'=>array(
                'id'=>15,
                'title'=>'Вдохновение. Обучение. Страница',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/education/education-page.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'education',
        'name'=>'Обучение',
        'files'=> array(
            'coupon.html'=>array(
                'id'=>16,
                'title'=>'Вдохновение. Обучение. Подарочный купон',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/education/coupon.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'education',
        'name'=>'Обучение',
        'files'=> array(
            'timetable.html'=>array(
                'id'=>17,
                'title'=>'Вдохновение. Обучение. Расписание',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/education/timetable.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'gallery',
        'name'=>'Галлерея',
        'files'=> array(
            'gallery.html'=>array(
                'id'=>18,
                'title'=>'Вдохновение. Галлерея',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/gallery/gallery.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'gallery',
        'name'=>'Галлерея',
        'files'=> array(
            'gallery-page.html'=>array(
                'id'=>19,
                'title'=>'Вдохновение. Галлерея. Фотографии',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/gallery/gallery-page.php',
                    'footer'=>'global/footer.php',
                    'popup'=>'inner/gallery/gallery-window.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'gallery',
        'name'=>'Галлерея',
        'files'=> array(
            'gallery-window.html'=>array(
                'id'=>20,
                'title'=>'Вдохновение. Галлерея. Фотография',
                'includes'=>array(
                    'css'=>'global/css.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/gallery/gallery-page.php',
                    'footer'=>'global/footer.php',
                    'popup'=>'inner/gallery/gallery-window.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'catalog.html'=>array(
                'id'=>21,
                'title'=>'Вдохновение. Магазин. Каталог',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/catalog.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'item-page.html'=>array(
                'id'=>22,
                'title'=>'Вдохновение. Магазин. Страница Товара',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/item-page.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'item-delivery.html'=>array(
                'id'=>23,
                'title'=>'Вдохновение. Магазин. Доставка Товара',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/item-delivery.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'cart.html'=>array(
                'id'=>24,
                'title'=>'Вдохновение. Магазин. Моя корзина',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/cart.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'order-step-one.html'=>array(
                'id'=>25,
                'title'=>'Вдохновение. Магазин. Форма заказа',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/order-step-one.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'order-step-two.html'=>array(
                'id'=>26,
                'title'=>'Вдохновение. Магазин. Форма заказа',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/order-step-two.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'order-step-three.html'=>array(
                'id'=>27,
                'title'=>'Вдохновение. Магазин. Форма заказа',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/order-step-three.php',
                    'footer'=>'global/footer.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'order-success.html'=>array(
                'id'=>28,
                'title'=>'Вдохновение. Магазин. Форма заказа',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/order-step-three.php',
                    'footer'=>'global/footer.php',
                    'popup'=>'inner/magazine/order-success.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'order-success.html'=>array(
                'id'=>28,
                'title'=>'Вдохновение. Магазин. Форма заказа',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/order-step-three.php',
                    'footer'=>'global/footer.php',
                    'popup'=>'inner/magazine/order-success.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'login-popup.html'=>array(
                'id'=>29,
                'title'=>'Вдохновение. Магазин. Форма заказа',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/order-step-three.php',
                    'footer'=>'global/footer.php',
                    'popup'=>'inner/magazine/login-popup.php'
                )
            )
        )
    ),
    array(
        /* каждый вложенный массив описывает директорию проекта */
        'folder'=>'magazine',
        'name'=>'Магазин',
        'files'=> array(
            'login-popup-cabinet.html'=>array(
                'id'=>30,
                'title'=>'Вдохновение. Магазин. Форма заказа',
                'includes'=>array(
                    'css'=>'global/magaz.php',
                    'javascript'=>'global/javascript.php',
                    'header'=>'global/header.php',
                    'content'=>'inner/magazine/order-step-three.php',
                    'footer'=>'global/footer.php',
                    'popup'=>'inner/magazine/login-popup-cabinet.php'
                )
            )
        )
    )
);
?>