/* by renegade */
$(document).ready(function(){
    // cart summ
    CartTotalPrice();
    /*$('.to_cart_btn').click(function(){
     var iSum = 0, totalNum = 0, totalSum = 0, totalNumTxt = 'товар';
     if($('.cart').hasClass('empty')) {
     $('.cart').addClass('complete').removeClass('empty');
     }
     else {
     var totalNum = parseInt($('.cart_desc a').html());
     var iSum = parseInt($('.cart_total').html().replace(' ','') );
     }

     var totalNum = totalNum+1;

     var iPrice = parseInt($('.itemPrice').html().replace(' ','') );
     var totalSum = iPrice+iSum;
     var aPrice = totalSum.toString().split('');
     var iL = aPrice.length;
     var iCount = 1;

     for(var i=iL-1; i>-0; i--) {
     var sSep = (iCount%3 === 0) ? ' ' : '';
     aPrice[i] = sSep+aPrice[i];
     iCount++;
     }

     var totalNumStr = totalNum.toString();
     var lastIndex = totalNumStr.length-1;
     var lastChar = totalNumStr[lastIndex];
     if(lastChar == 2 && totalNumStr != 12 || lastChar == 3 && totalNumStr != 13 || lastChar == 4 && totalNumStr != 14) {
     var totalNumTxt = 'товара';
     }
     else if(lastChar == 1 && totalNumStr != 11) {
     var totalNumTxt = 'товар';
     }
     else {
     var totalNumTxt = 'товаров';
     }

     $('.cart_desc a').html(totalNum+' '+totalNumTxt);
     $('.cart_total').html( aPrice.join('') );
     });*/
    $('#cartOrder .cartItemNum').bind("change keyup input click", function() {
        CartTotalPrice();
    });
    $('#cartOrder .cartItemNum').bind("focus", function() {
        if(this.value == '0') {
            this.value = ''
        }
    });
    $('#cartOrder .cartItemNum').bind("blur", function() {
        if(this.value == '') {
            this.value = '0'
        }
    });
    function CartTotalPrice() {
        var iSum = 0, totalNum = 0, totalSum = 0, totalNumTxt = 'товар';
        var item = $('.cartItem');
        item.each(function(){
            if( $(this).find('.cartItemNum').length>0 && $(this).find('.cartItemPrice').length>0 ) {
                var iVal = parseInt($(this).find('.cartItemNum').val() );
                if(!iVal) {
                    var iVal = 0;
                }
                var iPrice = parseInt($(this).find('.cartItemPrice').html().replace(' ','') );
                var iSum = iVal*iPrice;
                totalNum += iVal;

                totalSum += iSum;

                var iSum = iSum.toString().replace(/(\d{1,3})(?=((\d{3})*)$)/g, " $1");
                $(this).find('.cartItemSum').html(iSum);
            }
        });
        if(totalSum>0) {
            var aPrice = totalSum.toString().split('');
            var iL = aPrice.length;
            var iCount = 1;

            for(var i=iL-1; i>-0; i--) {
                var sSep = (iCount%3 === 0) ? ' ' : '';
                aPrice[i] = sSep+aPrice[i];
                iCount++;
            }

            var totalNumStr = totalNum.toString();
            var lastIndex = totalNumStr.length-1;
            var lastChar = totalNumStr[lastIndex];
            if(lastChar == 2 && totalNumStr != 12 || lastChar == 3 && totalNumStr != 13 || lastChar == 4 && totalNumStr != 14) {
                var totalNumTxt = 'товара';
            }
            else if(lastChar == 1 && totalNumStr != 11) {
                var totalNumTxt = 'товар';
            }
            else {
                var totalNumTxt = 'товаров';
            }

            $('.cartTotalNum').html(totalNum+' '+totalNumTxt);
            $('.cartTotalSum').html( aPrice.join('') );

            $('.cart').addClass('complete').removeClass('empty');
            $('.cart_desc a').html(totalNum+' '+totalNumTxt);
            $('.cart_total').html( aPrice.join('') );
            $('#cartOrder button').removeClass('disabled').removeAttr('disabled');
        }
        else {
            $('#cartOrder button').addClass('disabled').attr('disabled','disabled');
            $('.cartTotalNum').html('0 '+totalNumTxt);
            $('.cartTotalSum').html('0');

            $('#cart').removeClass('full').addClass('empty');
        }
    }
    //remove cart item
    $('.cart_item_delete').click(function(){
        var oRow = $(this).parents('.cartItem');
        oRow.css('overflow','hidden');
        oRow.find('td').css({'overflow':'hidden','background':'#fff'});
        oRow.animate({'opacity': 0},300,function(){
            oRow.find('td').css({'padding':0});
            oRow.animate({'height': 0},300);
            oRow.find('*').animate({'height': 0},300,function(){
                oRow.remove();
                CartTotalPrice();
            });
        });
    });

});