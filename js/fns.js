/* by renegade */
$(document).ready(function(){

    // Form value
    $('.search_fld, .sidebar_form .fld').click(function() {
        if (this.value == this.defaultValue) {
            this.value = '';
            $(this).css('fontStyle','normal');
        }
    });
    $('.search_fld, .sidebar_form .fld').blur(function() {
        if (this.value == '') {
            this.value = this.defaultValue;
            $(this).css('fontStyle','italic');
        }
    });

    // Newspaper Post: hidden <p> after More
    $('.newspaper_post .more').nextAll('p').css('display','none');
    // Newspaper Post: onClick show <p> after More
    $('.newspaper_post .pseudolink').click(function(){
        $(this).parents('.more').nextAll('p').slideDown(300);
        $(this).parents('.more').css('display','none');
        return false;
    });

    // FAQ toggle
    $('.faq_question').click(function() {
        $(this).parents('.faq_list').find(".show").toggleClass('show').find('.faq_answer').slideUp('slow'); // при клике раскрываем блок и закрываем другие видимые блоки
        $(this).parents('.faq_item').toggleClass('show').find('.faq_answer').slideToggle('slow');
        return false;
    });

    // custom select ikSelect
    $(".custom-select").ikSelect({
        autoWidth: false,
        ddFullWidth: false
    });

});